<?php
/**
 * Esta classe gera arquivos modelo para a nova arquitetura de sistemas que comecei
 * a usar desde 2016.
 * Esta classe gerar� arquivos de Controle, Vis�o, Neg�cio, Modelo e Dao.
 *
 * @author Alex Lunardelli
 * @date   22/03/2017
 */

include_once 'class.gerador.php';
include 'ArquivoVisaoPadrao2017.php';
include 'ArquivoDaoPadrao2017.php';
include 'ArquivoControlePadrao2017.php';
include 'ArquivoModeloPadrao2017.php';
include 'ArquivoNegocioPadrao2017.php';
include 'ArquivoFormularioPadrao2017.php';
include 'ArquivoValidadorFormularioPadrao2017.php';


class novaClasseMvc01 extends novaClasse {
  public function __construct() {
    parent::__construct();
    $this->sCabecalho  = "/************************************************** \n";
    $this->sCabecalho .= '* @package    '.$this->sProjeto ."\n";
    $this->sCabecalho .= '* @author     '.$this->sAutor.' <'.$this->sAutorEmail.'>'."\n";
//    $this->sCabecalho .= '* @copyright  '.$this->sDireitos."\n";
    $this->sCabecalho .= '* @date       '.$this->sDataAtual."\n";
    $this->sCabecalho .= '* @version    Arquitetura 2017'."\n";
    $this->sCabecalho.= " **************************************************/"."\n";
    
    
  }
  
  public function executa() {

    $this->selecionaBD();
    $this->buscaDados();
    
    $this->aCriarOsArquivos[] = new ArquivoVisaoPadrao2017($this);
    $this->aCriarOsArquivos[] = new ArquivoControlePadrao2017($this);
    $this->aCriarOsArquivos[] = new ArquivoNegocioPadrao2017($this);
    $this->aCriarOsArquivos[] = new ArquivoModeloPadrao2017($this);
    $this->aCriarOsArquivos[] = new ArquivoDaoPadrao2017($this);
    $this->aCriarOsArquivos[] = new ArquivoFormularioPadrao2017();
    $this->aCriarOsArquivos[] = new ArquivoValidadorFormularioPadrao2017($this);
    
    foreach ($this->aCriarOsArquivos as $oArquivo) {
      $oArquivo->gerar();
    }
  }
}
