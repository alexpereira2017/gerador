<?php
include_once 'ArquivosInterface.php';
class ArquivoDaoPadrao2017 implements ArquivosInterface{
  private $oNovaClasse;
  public function __construct(novaClasse $oNovaClasse) {
    $this->oNovaClasse = $oNovaClasse;
  }

  public function gerar() {
    $sConteudo = $this->montarDados();
    $this->oNovaClasse->fecharArquivo('Dao'.$this->oNovaClasse->getNomePadronizado().'.php',$sConteudo);
  }
 
  private function montarDados() {
    $sConteudo = '<?php
'.$this->oNovaClasse->sCabecalho.'
  include_once \'modulosPHP/modelo/Modelo'.$this->oNovaClasse->getNomePadronizado().'.php\';
  class Dao'.$this->oNovaClasse->getNomePadronizado().' {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once \'modulosPHP/ajudantes/ClassConexao.php\';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }'."\n";
    
    $sConteudo .= $this->gerarInsert();
    $sConteudo .= $this->gerarUpdate();
    $sConteudo .= $this->gerarDelete();
    $sConteudo .= $this->gerarSelect();
    $sConteudo .= $this->gerarExecutar();

    return $sConteudo;
  }
  
  private function gerarInsert() {
    $sConteudo = IND04.'public function inserir ( Modelo'.$this->oNovaClasse->getNomePadronizado().' $oModelo ) {
      $sSql = "INSERT INTO '.$this->oNovaClasse->sNomeClasse."(\n";

    for ($i = 1; $i < $this->oNovaClasse->iLinhasTabela; $i++) {
      // $sConteudo .= ($i == 1) ? '' : IND27;
      $sIdentacao = $this->oNovaClasse->calculaIndentacao($sNomeDoCampo);
      $sConteudo .= IND29.$this->oNovaClasse->NM_CAMPO[$i];
      $sConteudo .= ($i != $this->oNovaClasse->iLinhasTabela - 1) ? ',' : ')';
      $sConteudo .= "\n";
    }
    $sConteudo .= IND19.'VALUES (';
    
    for ($i = 1; $i < $this->oNovaClasse->iLinhasTabela; $i++) {
      $sConteudo .= ($i == 1) ? '' : IND27;
      $sNomeDoCampo = $this->oNovaClasse->gerarCamelCase($this->oNovaClasse->NM_CAMPO[$i]);
      $sIdentacao = $this->oNovaClasse->calculaIndentacao($sNomeDoCampo);
      $sConteudo .= $this->verificarCamposDataHoraCriacao($sNomeDoCampo);
      $sConteudo .= ($i != $this->oNovaClasse->iLinhasTabela - 1) ? ',' : ')";';
      $sConteudo .= "\n";
    }
    
    $sConteudo .= IND06.'return $this->executar( $sSql );'."\n";
    $sConteudo .= IND04.'}';
    $sConteudo .= "\n\n";
    return $sConteudo;
  }
  
  private function gerarUpdate() {
    $sConteudo = IND04.'public function editar ( Modelo'.$this->oNovaClasse->getNomePadronizado().' $oModelo, $sFiltro ) {'."\n";
    $sConteudo .= IND06.'$sSql = "UPDATE '.$this->oNovaClasse->sNomeClasse."\n";
    $sConteudo .= IND18.'SET ';

    for ($i = 1; $i < $this->oNovaClasse->iLinhasTabela; $i++) {
      $sConteudo .= ($i == 1) ? '' : IND22;
      $sNomeDoCampo = $this->oNovaClasse->gerarCamelCase($this->oNovaClasse->NM_CAMPO[$i]);
      $sIdentacao = $this->oNovaClasse->calculaIndentacao($sNomeDoCampo);
      $sConteudo .= $this->oNovaClasse->NM_CAMPO[$i];
      $sConteudo .= $sIdentacao;
      $sConteudo .= ' = \'".$oModelo->'.$sNomeDoCampo.'."\'';
      $sConteudo .= ($i != $this->oNovaClasse->iLinhasTabela - 1) ? ',' : '';
      $sConteudo .= "\n";
    }
    $sConteudo .= IND16.'".$sFiltro;'."\n";

    $sConteudo .= IND06.'return $this->executar( $sSql );'."\n";
    $sConteudo .= IND04."}";
    $sConteudo .= "\n\n";
    return $sConteudo;
  }
  
  private function gerarDelete() {
    $sConteudo = IND04.'public function remover ( $sFiltro ) {'."\n";
    $sConteudo .= IND06.'$sSql = "DELETE FROM '.$this->oNovaClasse->sNomeClasse .'". $sFiltro;'."\n";
    $sConteudo .= IND06.'return $this->executar( $sSql );'."\n";
    $sConteudo .= IND04."}";
    $sConteudo .= "\n\n";
    return $sConteudo;
  }
  
  private function gerarSelect() {
    $sConteudo = IND04.'public function listar ( $mOpcoes = \'\') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = \'SELECT ';
    for ($i = 0; $i < $this->oNovaClasse->iLinhasTabela; $i++) {
      $sConteudo .= ($i == 0) ? '' : IND22;
      $sConteudo .= $this->oNovaClasse->NM_CAMPO[$i];
      $sConteudo .= ($i != $this->oNovaClasse->iLinhasTabela - 1) ? ',' : '';
      $sConteudo .= "\n";
    }
      $sConteudo .= IND17.'FROM '.$this->oNovaClasse->sNomeClasse.'
       \'.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new Modelo'.$this->oNovaClasse->getNomePadronizado().'();'."\n";

    for ($i = 0; $i < $this->oNovaClasse->iLinhasTabela; $i++) {
      $sNomeDoCampo = $this->oNovaClasse->gerarCamelCase($this->oNovaClasse->NM_CAMPO[$i]);
      $sIdentacao = $this->oNovaClasse->calculaIndentacao($sNomeDoCampo);
      $sConteudo .= IND08.'$oModelo->'.$sNomeDoCampo;
      $sConteudo .= $sIdentacao.' = $mResultado[$i][\''.$this->oNovaClasse->NM_CAMPO[$i].'\'];'."\n";
    }
      
        $sConteudo .= IND08.'$this->aModelo[] = $oModelo;
      }

      return $this->aModelo;'."\n";
    $sConteudo .= IND04."}";
    $sConteudo .= "\n\n";
    return $sConteudo;    
  }

  private function verificarCamposDataHoraCriacao($strCampo) {
    $ret = '';
    switch ($strCampo) {
      case 'DtCriacao':
        $ret = 'curdate()';
        break;

      case 'HrCriacao':
        $ret = 'curtime()';
        break;
      
      default:
        $ret = '\'".$oModelo->'.$strCampo.'."\'';
        break;
    }
    
    return $ret;
  }
  
  private function gerarExecutar() {
    $sConteudo = IND04.'public function executar ( $sSql ) {'."\n";
    $sConteudo .= IND06.'if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = \'Ocorreu um erro ao salvar o registro.\';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = \'O registro foi salvo com sucesso!\';
        $bSucesso = true;
      }

      $this->aMsg = array(\'iCdMsg\' => $this->iCdMsg,
                            \'sMsg\' => $this->sMsg);
      return $bSucesso;
    }';
    $sConteudo .= "\n  }";
    $sConteudo .= "\n";
    return $sConteudo;    
  }
}