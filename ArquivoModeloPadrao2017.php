<?php
include_once 'ArquivosInterface.php';
class ArquivoModeloPadrao2017 implements ArquivosInterface{
  private $oNovaClasse;
  public function __construct(novaClasse $oNovaClasse) {
    $this->oNovaClasse = $oNovaClasse;
  }

  public function gerar() {
    $sConteudo = $this->montarDados();
    $this->oNovaClasse->fecharArquivo('Modelo'.$this->oNovaClasse->getNomePadronizado().'.php',$sConteudo);
  }
  
  private function montarDados() {
    $sNomeDaTabelaPadronizado = $this->oNovaClasse->getNomePadronizado();
$sConteudo = '<?php
'.$this->oNovaClasse->sCabecalho.'

  class Modelo'.$sNomeDaTabelaPadronizado.' { '."\n";

    for ($i = 0; $i < $this->oNovaClasse->iLinhasTabela; $i++) {
      $sConteudo .= IND04.'public $'.$this->oNovaClasse->gerarCamelCase($this->oNovaClasse->NM_CAMPO[$i]).";\n";
    }

    $sConteudo .= IND04.'public $sConteudo;'."\n";
    $sConteudo .= IND04.'public $aListaModelos;'."\n";
        
    
    
    $sConteudo .= "\n";
    $sConteudo .= IND04.'public function carregarDadosAposPost($aDados) { ';
    $sConteudo .= "\n";
    
    for ($i = 0; $i < $this->oNovaClasse->iLinhasTabela; $i++) {
      $sNomeDoCampo = $this->oNovaClasse->gerarCamelCase($this->oNovaClasse->NM_CAMPO[$i]);
      $sIdentacao = $this->oNovaClasse->calculaIndentacao($sNomeDoCampo);
      $sConteudo .= IND06.'$this->'.$sNomeDoCampo.'';
      $sConteudo .= $sIdentacao;
      $sConteudo .= ' = (isset($aDados[\'CMP'.$sNomeDaTabelaPadronizado.$sNomeDoCampo.'\']))'.$sIdentacao.' ? $aDados[\'CMP'.$sNomeDaTabelaPadronizado.$sNomeDoCampo.'\'] : \'\';';
      $sConteudo .= "\n";
    }


    $sConteudo .='
    }
  }';
    return $sConteudo;
  }
}
