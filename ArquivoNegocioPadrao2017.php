<?php
include_once 'ArquivosInterface.php';
class ArquivoNegocioPadrao2017 implements ArquivosInterface {
  private $oNovaClasse;
  public function __construct(novaClasse $oNovaClasse) {
    $this->oNovaClasse = $oNovaClasse;
  }
  
  public function gerar() {
    $sConteudo = $this->montarDados();
    $this->oNovaClasse->fecharArquivo('Negocio'.$this->oNovaClasse->getNomePadronizado().'.php',$sConteudo);
  }
  
  private function montarDados() {
    $sConteudo = '<?php
'.$this->oNovaClasse->sCabecalho.'
  include_once \'modulosPHP/dao/Dao'.$this->oNovaClasse->getNomePadronizado().'.php\';

  class Negocio'.$this->oNovaClasse->getNomePadronizado().' {
    public function __construct() {
      $this->oUtil = new wTools();
    }
    
    public function Salvar( Modelo'.$this->oNovaClasse->getNomePadronizado().' $obj ) {
    
    }
  
  }';
    return $sConteudo;
  }
}
