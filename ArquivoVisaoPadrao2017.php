<?php

include_once 'ArquivosInterface.php';
class ArquivoVisaoPadrao2017 implements ArquivosInterface {
  private $oNovaClasse;
  public function __construct(novaClasse $oNovaClasse) {
    $this->oNovaClasse = $oNovaClasse;
  }

  public function gerar() {
    $sConteudo = $this->montarDados();
    $this->oNovaClasse->fecharArquivo('Visao'.$this->oNovaClasse->getNomePadronizado().'.php',$sConteudo);
  }
  
  private function montarDados() {
$sConteudo = '<?php
include_once \'view.geral.php\';

class Visao'.$this->oNovaClasse->getNomePadronizado().' extends view_geral{
  protected $oModel; 

  public function __construct($oModel) {
    parent::__construct();
    $this->oModel = $oModel;
  }

  public function montarCorpoConteudo() {
 
  }
}';
  return $sConteudo;
    
  }
}
