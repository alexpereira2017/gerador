<?php

class wTools {

  public $sErro;
  public $aParametros;
  public $CFGpath;
  public $aMsg = array ('mResultado' => '',
                        'sMsg'       => '',
                        'sMsgErro'   => '');

  public function __construct() {
    $this->pathImagens = 'imagens/ilustrativas/';
    $this->sUrlBase = ($_SERVER['SERVER_NAME'] == 'localhost') ? 'http://localhost/lunacom' : 'http://www.lunacom.com.br';
  }


  /* wTools::aspas
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 07/10/2010
   *
   */
  public function aspas($sFrase) {
    ?>
      <p class="destaque-aspas">
        <img src="quote.gif" alt="quote" />
        <?php echo $sFrase ?>
      </p>
    <?php
  }

  /* wTools::subLinguagem
   * Recebe o texto de um editor contendo TAGs gen�ricas. Estas TAGs transforam
   * seu conte�do de forma como definida nos arrays.
   *
   * @param  $sConteudo                  - Conteudo a ser transformado
   * @return $this->ConteudoTransformado - Conteudo Transformado
   *
   * $aProcuraPor = array (abertura da tag da sub linguagem,
   *                       fechamento da tag da sub linguagem)
   *
   * $TrocaPor = array(abertura da tag em HTML,
                      fechamento da tag em HTML)
   *
   * @date 09/10/2010
   *
   * TAGs CRIADAS - FUNCAO:
   *
   * <aspas> </aspas> - Adiciona o texto de uma forma formatada. Da destaque a uma observa��o ou cita��o importante no contexto do assunto.
   *
   */
  public function subLinguagem($sConteudo) {
    $aProcuraPor = array('&lt;aspas&gt;',
                         '&lt;/aspas&gt;',
                         '&lt;ver&gt;',
                         '&lt;/ver&gt;'
            );    
    $aTrocaPor = array ('<div class="destaque-aspas"><img src="'.$this->pathImagens .'quote.gif" alt="quote" />',
                        '</div>',
                        '<span style="color:#F00">',
                        '</span>'
    );

    
    $sConteudo = str_replace($aProcuraPor, $aTrocaPor, $sConteudo);

    $this->ConteudoTransformado = $sConteudo;

    return $this->ConteudoTransformado;
    
  }
  /* wTools::novoComentario
   * Adiciona um comentario. Utilizado para paginas como noticias ou artigos.
   *
   * CSS
   *  - class="comentario-post"
   *  - class="titulo"
   *
   * @param  $sAction           - Documento que trata o formulario  
   * @param  $sNomeForm         - Nome do formulario
   * @param  $iRelacionamento   - Valor de um ID ao qual o comentario ira pertencer
   * @param  $sVarDestinoOpc    - Variavel opcional para o tratamento dos dados
   * @param  $sScriptValidacao  - Script para valida��o do formulario
   * @param  $sTitComentario    - Frase que fica no titulo que fica no topo do formulario
   * @return null
   *
   * @date 14/10/2010
   */ 

  public function novoComentario($sAction, $sNomeForm, $iRelacionamento, $sVarDestinoOpc, $sScriptValidacao = '', $sTitComentario='Deixe o seu coment�rio! ') {
    ?>
    <div class="comentario-post">
      <h3 class="titulo"><?php echo $sTitComentario; ?></h3>
        <form action="<?php echo $sAction ?>" name="<?php echo $sNomeForm ?>" method="POST" <?php echo $sScriptValidacao ? 'onSubmit="return '.$sScriptValidacao.'"' : '' ?> >
          <table width="435" border="0">
            <tr>
              <td>
                <input type="text" name="FRMcoment_autor" value="Nome" tabindex="1" class="FRM_coment" onblur="if (this.value == ''){this.value = 'Nome';}" onfocus="if (this.value == 'Nome') {this.value = '';}" />
              </td>
            </tr>
            <tr>
              <td>
                <input type="text" name="FRMcoment_email" value="Email" tabindex="1" class="FRM_coment" onblur="if (this.value == ''){this.value = 'Email';}" onfocus="if (this.value == 'Email') {this.value = '';}" />
              </td>
            </tr>
            <tr>
              <td>
                <input type="text" name="FRMcoment_site" value="http://www.lunacom.com.br" tabindex="1" class="FRM_coment" onblur="if (this.value == ''){this.value = 'http://www.lunacom.com.br';}" onfocus="if (this.value == 'http://www.lunacom.com.br') {this.value = '';}" />
              </td>
            </tr>
            <tr>
              <td>
                <textarea name="FRM_comentario" rows="8" cols="10" tabindex="4" class="FRM_textarea" onfocus="if (this.value == 'Seu Coment�rio...') {this.value='';}" onblur="if (this.value == ''){this.value = 'Seu Coment�rio...';}">Seu Coment�rio...</textarea>
              </td>
            </tr>
            <tr>
              <td>
                <input type="hidden" name="sAcao" value="<?php echo $sVarDestinoOpc ?>" />
                <input type="hidden" name="id_relacionamento" value="<?php echo $iRelacionamento ?>"  />
                <input type="submit" value="Enviar" />
              </td>
            </tr>
          </table>
        </form>
    </div>
  <?PHP
  }

  /* wTools::anti_sql_injection
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 23/10/2010
   * @param  string $sStr - String a ser analisada
   * @return string $sStr - String pronta
   */

  public function anti_sql_injection ($sStr) {
		if (!is_numeric($sStr)) {
			$sStr= get_magic_quotes_gpc() ? stripslashes($sStr) : $sStr;
			$sStr= function_exists("mysql_real_escape_string") ? mysql_real_escape_string($sStr) : mysql_escape_string($sStr);
		}
		return $sStr;
	}
  
  /* wTools::nomeMes
   *
   * Traz o nome do m�s em Portugues
   * @date 31/10/2010
   * @param  integer $iMes    - Numero do mes a ser testado
   * @return string $sNomeMes - Nome do mes em portugues
   */

  public function nomeMes($iMes){

		switch ($iMes){
			case 01: $sNomeMes = 'Janeiro'; break;
			case 02: $sNomeMes = 'Fevereiro';break;
			case 03: $sNomeMes = 'Mar&ccedil;o';break;
			case 04: $sNomeMes = 'Abril';break;
			case 05: $sNomeMes = 'Maio';break;
			case 06: $sNomeMes = 'Junho';break;
			case 07: $sNomeMes = 'Julho';break;
			case 08: $sNomeMes = 'Agosto';break;
			case 09: $sNomeMes = 'Setembro';break;
			case 10: $sNomeMes = 'Outubro';break;
			case 11: $sNomeMes = 'Novembro';break;
			case 12: $sNomeMes = 'Dezembro';break;
      default: $sNomeMes = '';
		}
		return $sNomeMes;
	}

  /*    IMPLEMENTAR
   *
   *  wTools::novoForm
   * Adiciona um comentario. Utilizado para paginas como noticias ou artigos.
   *
   * CSS
   *  - class="formNomeInterno"
   *  - class="titulo"
   *
   * @param  $sAction           - Documento que trata o formulario
   * @param  $sNomeForm         - Nome do formulario
   * @param  $sNomeForm         - Array contando os campos que deverao ser exibidos
   * @param  $iRelacionamento   - Valor de um ID ao qual o comentario ira pertencer
   * @param  $sVarDestinoOpc    - Variavel opcional para o tratamento dos dados
   * @param  $sScriptValidacao  - Script para valida��o do formulario
   * @param  $sTitComentario    - Frase que fica no titulo que fica no topo do formulario
   * @return null
   *
   * @date 14/10/2010
   */
/*
  public function formNomeInterno($sAction, $sNomeForm, $aConfig = '', $iRelacionamento, $sVarDestinoOpc, $sScriptValidacao = '', $sTitForm = '') {




    ?>
    <div class="formNomeInterno">
      <h3 class="titulo"><?php echo $sTitForm ?></h3>
        <form action="<?php echo $sAction ?>" name="<?php echo $sNomeForm ?>" method="post" <?php echo $sScriptValidacao ? 'onSubmit="return '.$sScriptValidacao.'"' : '' ?> >
          <table width="435" border="0">
          <?php if(isset($aConfig['nome'])) {?>
            <tr>
              <td>
                <input type="text" name="FRMcoment_autor" value="Nome" tabindex="1" class="FRM_coment" onblur="if (this.value == ''){this.value = 'Nome';}" onfocus="if (this.value == 'Nome') {this.value = '';}" />
              </td>
            </tr>
          <?php } ?>
            <tr>
              <td>
                <input type="text" name="FRMcoment_email" value="Email" tabindex="1" class="FRM_coment" onblur="if (this.value == ''){this.value = 'Email';}" onfocus="if (this.value == 'Email') {this.value = '';}" />
              </td>
            </tr>
            <tr>
              <td>
                <input type="text" name="FRMcoment_site" value="http://www.lunacom.com.br" tabindex="1" class="FRM_coment" onblur="if (this.value == ''){this.value = 'http://www.lunacom.com.br';}" onfocus="if (this.value == 'http://www.lunacom.com.br') {this.value = '';}" />
              </td>
            </tr>
            <tr>
              <td>
                <textarea name="FRM_comentario" rows="8" cols="10" tabindex="4" class="FRM_textarea" onfocus="if (this.value == 'Seu Coment�rio...') {this.value='';}" onblur="if (this.value == ''){this.value = 'Seu Coment�rio...';}">Seu Coment�rio...</textarea>
              </td>
            </tr>
            <tr>
              <td>
                <input type="hidden" name="funcao" value="<?php echo $sVarDestinoOpc ?>" />
                <input type="hidden" name="id_relacionamento" value="<?php echo $iRelacionamento ?>"  />
                <input type="submit" value="Enviar" />
              </td>
            </tr>
          </table>
        </form>
    </div>
  <?PHP

  }

*/

  /* wTools::paginador
   *
   *
   * @date 02/11/2010
   * @param int    $iPgAtual         - Recebe o ID da pagina atual
   * @param string $sUrl             - Recebe um endere�o onde vai ser colocado um ID ex.: '<a class="paginacao" href="index.php?sec=noticias&ind=' ;
   * @param string $sUrlAtual        - Recebe um endereco onde vai ser colocado o ID, igual ao item 2, porem tem uma classe diferente para ser usada e
   *                                   diferenciar a p�gina atual.
   *                                   ex.: ' <a class="sPgAtual" href="index.php?sec=noticias&ind= ';   *
   * @param string $sQuery           - Faz a consulta no banco de dados para saber o total de linhas que uma tabela tem
   *                          	       ex.: SELECT COUNT(id_noticia) FROM noticias WHERE ativa = 1
   * @param int    $iQnt             - Quantidade de linhas por p�gina
   * @param string $sRedir           - Local para qual o paginador vai apontar em caso de erro
   *
   * @return int $iNumPaginas        - Retorna o numero total de dados no banco
   */
  public function paginador($iPgAtual, $sUrl, $sUrlAtual, $sQuery, $iQnt = '10', $sRedir = ''){

    $oRes        = mysql_query($sQuery);
    $iNumRows    = mysql_num_rows($oRes);
    $iNumPaginas = ceil ($iNumRows / $iQnt) ;

    $pg_ultima = $iNumPaginas;

    // A p�gina atual que vem pela URL n�o pode ser um numero maior do que a quantidade de p�ginas que realmente existe    
    if($iPgAtual > $iNumPaginas) {
      header('Location:'.$sRedir);
      exit;
    }

    if ($iNumPaginas == 1){
      return  $iNumPaginas;
    }

    #escreve -- PRIMEIRA -- sempre vai ter o indice = 1
    echo $sUrl.'1"  > Primeira </a>&nbsp;';

    if ($iPgAtual == 1) {
      echo $sUrlAtual.$iPgAtual.'" > '.$iPgAtual.' </a>&nbsp;';
      echo $sUrl.'2"  > 2 </a>&nbsp; ';
      if ($pg_ultima >= 3 ) echo $sUrl.'3"  > 3 </a>&nbsp; ';
      if ($pg_ultima >= 4 ) echo $sUrl.'4"  > 4 </a>&nbsp; ';
      if ($pg_ultima >= 5 ) echo $sUrl.'5"  > 5 </a>&nbsp; ';

      }elseif ($iPgAtual == 2){
        echo $sUrl.'1"  > 1 </a>&nbsp; ';
        echo $sUrlAtual.$iPgAtual.'" >'.$iPgAtual.' </a>&nbsp;';
        if ($pg_ultima >= 3 ) echo $sUrl.'3"  > 3 </a>&nbsp; ';
        if ($pg_ultima >= 4 ) echo $sUrl.'4"  > 4 </a>&nbsp; ';
        if ($pg_ultima >= 5 ) echo $sUrl.'5"  > 5 </a>&nbsp; ';

        }elseif ($iPgAtual == ($iNumPaginas - 1)){
          if ( ($iPgAtual - 3) > 0 ) echo $sUrl.($iPgAtual - 3).'" > '.($iPgAtual - 3).' </a>&nbsp;';
          if (($iPgAtual - 2) > 0 ) echo $sUrl.($iPgAtual - 2).'" > '.($iPgAtual - 2).' </a>&nbsp;';
          echo $sUrl.($iPgAtual - 1).'" > '.($iPgAtual - 1).' </a>&nbsp; ';
          echo $sUrlAtual.$iPgAtual.'" > '.$iPgAtual.' </a>&nbsp;';
          echo $sUrl.($iPgAtual + 1).'" > '.($iPgAtual + 1).' </a>&nbsp;';

          }elseif ($iPgAtual == $iNumPaginas) {
            if (($iPgAtual - 4) > 0 ) echo $sUrl.($iPgAtual - 4).'" > '.($iPgAtual - 4).' </a>&nbsp;';
            if (($iPgAtual - 3) > 0 ) echo $sUrl.($iPgAtual - 3).'" > '.($iPgAtual - 3).' </a>&nbsp;';
            echo $sUrl.($iPgAtual - 2).'" > '.($iPgAtual - 2).' </a>&nbsp;';
            echo $sUrl.($iPgAtual - 1).'" > '.($iPgAtual - 1).' </a>&nbsp; ';
            echo $sUrlAtual.$iPgAtual.'" > '.$iPgAtual.' </a>&nbsp;';

            }else{
              echo $sUrl.($iPgAtual - 2).'" > '.($iPgAtual - 2).' </a>&nbsp;';
              echo $sUrl.($iPgAtual - 1).'" > '.($iPgAtual - 1).' </a>&nbsp; ';
              echo $sUrlAtual.$iPgAtual.'" > '.$iPgAtual.' </a>&nbsp;';
              if ($pg_ultima >= ($iPgAtual + 1) ) echo $sUrl.($iPgAtual + 1).'" > '.($iPgAtual + 1).' </a>&nbsp;';
              if ($pg_ultima >= ($iPgAtual + 2) ) echo $sUrl.($iPgAtual + 2).'" > '.($iPgAtual + 2).' </a>&nbsp; ';
          }

    #escreve -- �LTIMA -- sempre vai ter o indice = ultima pagina
    echo $sUrl.$pg_ultima.'" > �ltima </a>&nbsp; 	';


    return $iNumPaginas;
	}

  /* wTools::validarIndexPaginacao
   *
   * Evita que o usu�rio digite um numero de p�ginas superior ao que realmente
   * existe no sistema quando � utilizado o paginador
   * @date 18/12/2012
   * @param  int    $iPgAtual          - Vem geralmente da vari�vel $_GET['n'] da pagina��o.
   *                                     N�mero da pagina��o atual que esta visivel da url
   * @param  int    $iQntItensListagem - Quantidade de itens listados por p�gina
   * @param  string $sPgRetEmErro      - P�gina em que ser� direcionado em caso de erro
   * @param  string $sTbPaginacao      - Tabela paginacao
   * @return true em caso de suceeso
   */
  public function validarIndexPaginacao($iPgAtual, $iQntItensListagem, $sPgRetEmErro, $sTbPaginacao) {
    $iIndice  = $iPgAtual - 1;
    $iIndice  = $iIndice * $iQntItensListagem ;   
    $this->pegaInfoDB($sTbPaginacao, 'count(1)');

    $iNumMaxPaginas = ceil ($this->RETDB[0][0] / $iQntItensListagem) ;
    if ($iPgAtual > $iNumMaxPaginas) {
      //echo '<h3>asd'.$iPgAtual .' -- '. $iNumMaxPaginas  .' '.$sPgRetEmErro.'</h3>';
      header('location:'.$sPgRetEmErro);
    }
    return true;
  }

  /* wTools::resumo
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 02/11/2010
   * @param  string $sParagrafo - String a ser analisada
   * @param  int    $iResQntCar - Quantidade de caracteres que ser�o apresentados
   * @return string $sResumida  - String pronta
   */
  public function resumo($sParagrafo, $iResQntCar = '150'){
  $sResumida = substr($sParagrafo , 0 , $iResQntCar);
  $sCortada  = strrpos($sResumida, " ");
  $sResumida = substr($sParagrafo , 0, $sCortada);

  return $sResumida.' ...';
  }


  public function enviaEmail() {

    # Monta as vari�veis usadas

		$this->sRemetente     = strip_tags($this->sRemetente);
    if(isset($this->sDestinatario)) {
      $this->sDestinatario  = strip_tags($this->sDestinat�rio);
    }
    $this->sAssunto       = strip_tags($this->sAssunto);
    $this->sMensagem      = $this->sMensagem;

    $sHeaders  = "MIME-Version: 1.0\r\n";
    $sHeaders .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $sHeaders .= 'From: '.$this->sRemetente;

    if(isset($this->aDestinatarios)) {
      foreach ($this->aDestinatarios as $sDestinatario) {
        $sDestinatario = strip_tags($sDestinatario);
        $bResult = mail( $sDestinatario, $this->sAssunto , $this->sMensagem, $sHeaders);
      }
    } else {
      $bResult = mail( $this->sDestinatario , $this->sAssunto , $this->sMensagem, $sHeaders);
    }

    if(!$bResult) {
      return false;
    }

    return true;
  }

  /* wTools::enviaImg
   *
   * Upload de Imagens
   * @date 24/11/2010
   * @param  array   $aArquivo        - Um array com o arquivo ex.: $_FILES["img_capa"]
   * @param  string  $sLocal          - Local a salvar a imagem
   * @param  string  $iLargura        - Largura da imagem
   * @param  string  $iAltura         - Altura da imagem
   * @param  string  $bNomeAleatorio  - Coloca um nome aleatorio para a imagem
   * @param  string  $bApagarImg      - Faz upload de uma imagem para o lugar de uma outra
   * @return string  $sNomeImg        - Nome da imagem salva
   * @return string  $iSize           - Tamanho m�ximo da imagem
   */
	public function enviaImg($aArquivo, $sLocal, $iLargura, $iAltura, $bNomeAleatorio = false, $bApagarImg = false, $iSize = 1000000) {

	$modTamanho = '';
	$sNomeImg = 1;

	if($aArquivo["name"] == ''){
		$this->sMsg = "Por favor, selecione uma figura";
		echo '
		<script type="text/javascript">
			alert("'.$this->sMsg.'");
		</script>
		';
		return $sNomeImg;
	}

		if ((($aArquivo["type"] == "image/gif") || ($aArquivo["type"] == "image/jpeg") || ($aArquivo["type"] == "image/jpg"))	&& ($aArquivo["size"] < $iSize)) {
      if ($aArquivo["error"] == 0) {

        $aDimensoes = getimagesize($aArquivo["tmp_name"]);

        if( ($aDimensoes[0] <= $iLargura || ($aDimensoes[1] <= $iAltura)) ) {

          // Pega extens�o do arquivo
          preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $aArquivo["name"], $aExtensao);

          // Nome da imagem
          if($bNomeAleatorio) {
            $sNomeImg = md5(uniqid(time())) . "." . $aExtensao[1];
          } else {
            $sNomeImg = $aArquivo["name"];
          }

          // Caminho de onde a imagem ficar�
          $imagem_dir = $sLocal . $sNomeImg;

          // Faz o upload da imagem
          move_uploaded_file($aArquivo["tmp_name"], $imagem_dir);

          //mensagem de sucesso
          $this->sMsg = "Sua foto foi enviada com sucesso!";
          //aviso($this->sMsg);
          return $sNomeImg;

        } else {
          //mensagem de erro
          //aviso ('Insira uma imagem de no m�ximo '.$iLargura.' x '.$iAltura.' pixels');
          $this->sMsg  = '� necess�rio inserir uma imagem com tamanho m�ximo de '.$iLargura.' x '.$iAltura.'px';
          $this->sErro = 'Erro';
          return $sNomeImg;
        }
      } else {
        //mensagem de erro
        $this->sMsg = 'Erro no carregamento da imagem: '.$aArquivo["error"];
        $this->sErro = 'Erro';
        return $sNomeImg;
      }
		}else{
      //mensagem de erro
      $this->sMsg = "A imagem n�o pode ser enviada com sucesso.";
      $this->sErro = 'Erro';
      return $sNomeImg;
		}

	}

  /* wTools::getSeguro
   *
   * Testa se o conte�do recebido via GET � o esperado
   * @date 14/12/2010
   * @param  string  $iId            - Id a testar
   * @return string  $iIndice        - Retorna o numero referente ao indice ou 0 caso seja malicioso.
   */
  public function getSeguro($iId) {
    // Testa se � a pagina principal ou uma sub-pagina
    if  ((isset($iId)) && (is_numeric($iId)))  {
      $iIndice = $iId;
    } else {
      $iIndice = 0;
    }
    return $iIndice;
  }

  /* wTools::uploadArquivos
   *
   * Upload de arquivos
   * @date 15/01/2011
   * @param  file    $_FILES          - Array de dados recebido de um campo do formul�rio. Ex.: $_FILES['CMPlogotipo']
   * @param  array   $$aConfigUpload  - Dados de configura��es de mensagens, formato dos arquivos, tamanho, pasta a ser salva etc.
   * @param  integer $iTamMax         - Tamanho m�ximo do arquivo em MB
   * @param  array   $aExtensoes      - Contem as extensoes de arquivos aceitos para upload
   * @param  bool    $bRenomear       - Renomear arquivo com nome aleat�rio
   * @return true
   *
   */
  public function uploadArquivos($aFiles, $aConfigUpload){

    // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
    if ($aFiles['error'] != 0) {
      die("N�o foi poss�vel fazer o upload, erro:<br />" . $aConfigUpload['erros'][$aFiles['arquivo']['error']]);
      exit; // Para a execu��o do script
    }

    // Caso script chegue a esse ponto, n�o houve erro com o upload e o PHP pode continuar

    // Faz a verifica��o da extens�o do arquivo
    $sExtensao = strtolower(end(explode('.', $aFiles['name'])));
    if (array_search($sExtensao, $aConfigUpload['extensoes']) === false) {
      echo "Por favor, envie arquivos com as seguintes extens�es: ";
    }

    // Faz a verifica��o do tamanho do arquivo
    else if ($aConfigUpload['tamanho'] < $aFiles['size']) {
      echo "O arquivo enviado � muito grande, envie arquivos de at� $iTamMax.";
    
    // O arquivo passou em todas as verifica��es, hora de tentar mov�-lo para a pasta
    } else {

      // Primeiro verifica se deve trocar o nome do arquivo
      if ($aConfigUpload['renomeia'] == true) {
      // Cria um nome baseado no UNIX TIMESTAMP atual e com extens�o .jpg
      $nome_final = time().'.jpg';
      } else {

      // Mant�m o nome original do arquivo
      $nome_final = $aFiles['name'];
      }

      // Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
      if (move_uploaded_file($aFiles['tmp_name'], $aConfigUpload['pasta'] . $nome_final)) {
        // Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
        echo "Upload efetuado com sucesso!";
        echo '<br /><a href="' . $aConfigUpload['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
      } else {
        // N�o foi poss�vel fazer o upload, provavelmente a pasta est� incorreta
        echo "N�o foi poss�vel enviar o arquivo, tente novamente";
      }
    }
  }

  /* wTools::subArray
   *
   * Divide um array em partes
   * @date 27/02/2011
   * @param  array     $aDados         - Array a ser dividido
   * @param  integer   $iAgrupa        - Quantidade de registros que cada indice do array ter�
   * @return array     $aArrayDividido - Resultato dividido

   */
  public function subArray($aDados, $iAgrupa) {
    $iQnt = count($aDados);
    $iRepete = ceil($iQnt / $iAgrupa);
    $iOffSet = 0;

    for ($i = 0; $i < $iRepete; $i++) {
      $aArrayDividido[] = array_slice($aDados, $iOffSet, 4);
      $iOffSet += $iAgrupa;
    }

    return $aArrayDividido;
  }

  /* wTools::montaUrlAmigavel
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 13/12/2010
   * @param  string $sFrase - String a ser analisada
   * @return string $sRet   - String pronta
   */
    public function montaUrlAmigavel($sFrase = '') {
      if($sFrase == '') {
        return false;
      }
      $sFrase = trim($sFrase);
      $sFrase = strtolower($sFrase);
      
      $sFrase = str_replace(' ', '-', $sFrase);
      $sFrase = ereg_replace("[����]","a",$sFrase);
      $sFrase = ereg_replace("[���]","e",$sFrase);
      $sFrase = ereg_replace("[��]","i",$sFrase);
      $sFrase = ereg_replace("[�����]","o",$sFrase);
      $sFrase = ereg_replace("[���]","u",$sFrase);
      $sFrase = ereg_replace("[.;:/\@#%&=?!$*,%(){}+']","",$sFrase);
      $sFrase = str_replace('--', '-', $sFrase);
      $sFrase = str_replace("�","c",$sFrase);
            
      // Algumas vezes o link termina com -
      $sFrase = ereg_replace("-$","",$sFrase);
      $sFrase = $sFrase.'/';
      $sRet = $sFrase;

      return $sRet;
    }

  /* wTools::montaSelect
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 03/05/2011
   * @param  string $sNome    - String a ser analisada
   * @param  array  $aValores - Array com os valores (value, nome)
   * @param  bool   $bId      - Se tiver id, repete o nome do campo
   * @param  string $sClass   - Nome da classe
   * @return true
   */
   
   public function montaSelect($sNome, $aValores, $sSelecionado = '', $bId = true, $sClass = '', $sJsAdicional = '', $sBranco = 'Selecione um item') { ?>

    <select name="<?php echo $sNome; ?>" <?php echo ($bId) ? 'id="'.$sNome.'"' : ''; echo ($sClass) ? 'class="'.$sClass.'"' : ''; ?> >
    <?php
      if($sBranco) { ?>
        <option <?php echo ($sSelecionado) ? '' : 'selected="selected"' ?>value=""><?php echo $sBranco; ?></option>
        <?php
      }
      foreach ($aValores as $sKey => $sValue) { ?>
        <option <?php echo ($sSelecionado == $sKey) ? 'selected="selected"' : '' ?>value="<?php echo $sKey; ?>"><?php echo $sValue; ?></option>
        <?php
      }
    ?>
    </select>
  <?php
   }

  /* wTools::msgRetAlteracoes
   *
   * Mensagem de retorno ao usu�rio ap�s altera��es realizadas em registros
   * @date 07/05/2011
   * @param mixed  $mResultado  - Tipo de resultado pode ser um codigo (0, 1, 2) ou uma mensagem tipo 'sucesso'
   * @param string $sMsg        - Mensagem para apresenta��o na tela
   * @param string $sMsgErro    - Mensagem de erro caso aconte�a
   * @param bool   $bMarcaVazio - Preenche com uma div o espa�o euquando n�o h� mensagem a ser exibida de retorno ao usu�rio
   * @return true
   */
  public function msgRetAlteracoes($mResultado = '', $sMsg = '', $sMsgErro = '', $bMarcaVazio = true) {

    if(isset($this->sResultado)) {
      $mResultado = $this->sResultado;
    }
    if(!isset($mResultado)) {
      if($bMarcaVazio) {?>
        <div class="msg-vazio">&nbsp;</div> <?php
      }
      return false;
    }
    if(isset($this->sMsg)) {
      $sMsg = $this->sMsg;
    }

    if(isset($this->sErro)) {
      $sMsgErro = $this->sErro;
    }

    switch ($mResultado) {
      case 'sucesso':
      case '0':?>
      <div class="msg-ok"><?php echo $sMsg ?></div> <?php
      break;

      case 'erro':
      case '1':?>
      <div class="msg-erro"><?php echo $sMsg; echo ($sMsgErro) ? ' - '.$sMsgErro : '' ?></div><?php
      break;

      case 'atencao':
      case '2':?>
      <div class="msg-atencao"><?php echo $sMsg; echo ($sMsgErro) ? ' - '.$sMsgErro : '' ?></div><?php
      break;

      default: ?>
        <div class="msg-vazio">&nbsp;</div> <?php
        break;
    }
    return true;
  }

  /* wTools::valida_sAcao
   *
   * Valida as possiveis acoes permitidas dentro das p�ginas de admin.
   * @date 07/05/2011
   * @param string $sAcao  - novo ou editar
   * @param string $sRedir - P�gina para redirecionar em caso de erro
   * @return $sAcao
   */
  public function valida_sAcao($sAcao, $sRedir) {
    $bRedirSeguranca = false;
    if(isset($sAcao)) {
      if($sAcao != 'novo' && $sAcao != 'editar') {
        $bRedirSeguranca = true;
      }
    } else {
      $bRedirSeguranca = true;
    }
    if($bRedirSeguranca) {
      header("Location: $sRedir");
      exit();
    }
    return $sAcao;
  }

  /* wTools::validarPreenchimento
   *
   * Validar os campos que devem ter seu preenchimento obrigat�rio
   * @date 08/05/2011
   * @param array $aCampos  - Campos a serem validados
   *                          ex: $aValidar = array (0 => array('T�tulo'     , $_POST['CMPtitulo']),
                                                     1 => array('Solicitante', $_POST['CMPsolicitante']),
                                                     3 => array('Descri��o'  , $_POST['CMPdesc']) );
   * @return true
   */
  public function valida_Preenchimento($aCampos) {

    if(!is_array($aCampos)) {
      return false;
    }
    foreach ($aCampos as $aCampo) {

      if($aCampo[1] == '') {
        return $aCampo[0];
      }
    }

    return true;

  }

  /* wTools::redirFRM
   *
   * Redireciona uma p�gia utilizando Javascript e enviando dados atrav�s de formul�rio
   * @date 08/05/2011
   * @param string $sUrl     - Endere�o da p�gina de destino.
   * @param array  $aCampos  - Campos a serem enviados pelo formulario. Indice do array = nome do campo.
   * @return true
   */
  public function redirFRM($sUrl, $aCampos = array()) { ?>
      <form id="FRMredir" method="post" action="<?php echo $sUrl ?>">
        <?php
          foreach ($aCampos as $sIndice => $sValor) { ?>
            <input type="hidden" name="<?php echo $sIndice; ?>" value="<?php echo $sValor; ?>" /> <?php
          }
        ?>        
      </form>          
      <script type="text/javascript">
        document.forms["FRMredir"].submit();
      </script>
    <?php
  }

 /* wTools::pegaInfoDB
   *
   * Faz uma consulta gen�rica ao banco de dados
   * @date 21/05/2011
   * @param string $sTabela  - Endere�o da p�gina de destino.
   * @param mixed  $mCampos  - Campos a serem enviados pelo formulario. Indice do array = nome do campo.
   * @param string $sWhere   - Filtro where
   * @return true
   */
  public function pegaInfoDB($sTabela, $mCampos, $sWhere = '') {

    if(is_array($mCampos)) {
      $sCampos = implode(',', $mCampos);
      $iQntCampos = count($mCampos);
    } else {
      $sCampos = $mCampos;
      $iQntCampos = 1;
    }

    $sQuery = 'SELECT '.$sCampos.'
                 FROM '.$sTabela. ' '.$sWhere;
    
    $sResultado = mysql_query($sQuery);
    if (!$sResultado) {
      die('Erro ao utilizar m�todo pegaInfoDB: ' . mysql_error());
      return false;
    }

    $this->iLinhas = mysql_num_rows($sResultado);

    while ($aResultado = mysql_fetch_row($sResultado)) {
      $this->RETDB[] = $aResultado;
      $aRetDB = $aResultado;
    }
    return $aRetDB;
  }

  /* wTools::montaSelectDB
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 21/05/2011
   * @param  string $sNome    - String a ser analisada
   * @param  array  $aValores - Array com os valores (value, nome)
   * @param  bool   $bId      - Se tiver id, repete o nome do campo
   * @param  string $sClass   - Nome da classe
   * @return true
   */

   public function montaSelectDB($sNome, $sTabela, $sChave, $sRotulo, $sSelecionado = '', $bId = true, $sClass = '', $sJsAdicional = '', $sBranco = 'Selecione um item', $sWhere = '') { ?>

    <select name="<?php echo $sNome; ?>" <?php echo ($bId) ? 'id="'.$sNome.'"' : ''; echo ($sClass) ? 'class="'.$sClass.'"' : ''; ?> >
    <?php
      if($sBranco) { ?>
        <option <?php echo ($sSelecionado) ? '' : 'selected="selected"' ?>value=""><?php echo $sBranco; ?></option>
        <?php
      }

      $sQuery = 'SELECT '.$sChave.'  AS chave,
                        '.$sRotulo.' AS rotulo
                   FROM '.$sTabela.'
                        '.$sWhere;
      $sResultado = mysql_query($sQuery);
      
      if (!$sResultado) {
        die('Erro ao utilizar o m�todo montaSelectDB: ' . mysql_error());
        return false;
      }

      while ($aResultado = mysql_fetch_array($sResultado)) { ?>
        <option <?php echo ($sSelecionado == $aResultado['chave']) ? 'selected="selected"' : '' ?>value="<?php echo $aResultado['chave']; ?>"><?php echo $aResultado['rotulo']; ?></option> <?php
      }
    ?>
    </select>
  <?php
   }

  /* wTools::montaLink
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 18/07/2011
   * @update 27/11/2011 - Atualizada a forma de buscar o path do link
   * @param  string $sTexto  - String a ser analisada
   * @param  string $sLink   - Link da p�gina
   * @param  string $CFGpath - 
   * @param  string $sClass  - Inserir uma classe especifica para o link
   * @param  bool   $bImprimir - Escrever o link na tela
   * @return true
   */
  public function montaLink($sTexto, $sLink, $CFGpath = '', $bImprimir = true, $sClass = '') {

    if(isset($this->sUrlBase) && $CFGpath == '') {
      $CFGpath = $this->sUrlBase.'/'  ;
    }
    
    if($bImprimir) {
      ?>
      <a href="<?php echo $CFGpath; ?><?php echo $sLink; ?>" <?php echo 'class="'.$sClass.'"'; ?>><?php echo $sTexto;?></a>
      <?php
    }

    return '<a href="'.$CFGpath.$sLink.'"><'.$sTexto.'</a>';
  }

  /* wTools::parseValue
   *
   * Configura valores de formas a serem utilizados em locais diferentes
   * @date 19/11/2011
   * @param  string $sCampo   - Valor a ser formatado
   * @param  string $sFormato - Formato a ser configurado o valor
   * @return $sRetCampo       - Valor a ser usado para inserir no banco de dados
   */
  public function parseValue($sCampo, $sFormato) {
    if($sCampo == '' || $sCampo == null) {
      return '';
    }
    if($sFormato == 'dt-bd') {
      $aResult = explode('/', $sCampo);
      $sRetCampo = $aResult[2].'-'.$aResult[1].'-'.$aResult[0];
    }


    //http://www.php.net/manual/pt_BR/security.variables.php
//define('REG_DATE'          , '([[:digit:]]{4})-([[:digit:]]{2})-([[:digit:]]{2})');
//define('REG_DIGIT_SIGNED'  , '^[-[:digit:]]+$');
//define('REG_DIGIT_UNSIGNED', '^[[:digit:]]+$');
//define('REG_PASSWORD'      , '^[[:alnum:]]+$');
//define('REG_TEXT'          , '[[:graph:][:blank:]]+');
//define('REG_WORD'          , '^[[:alpha:]]+$');

    return $sRetCampo;
  }


  /* wTools::pegarExtensao
   *
   * Retorna a extensao da string passada
   * @date 19/11/2011
   * @param  string $sNome - Campo com um nome contendo extensao  
   * @return $sExt         - Extensao do arquivo
   */
  public function pegarExtensao($sNome) {
    $sExt = strtolower(end(explode('.', $sNome)));
    return $sExt;
  }


 /* wTools::buscarInfoDB
   *
   * Faz uma consulta gen�rica ao banco de dados, similar ao metodo pegaInfoDB, por�m recebe uma string
   * de consulta SQL ao inves de nomes especificos de tabela, campos e filtro
   * @date 07/11/2011
   * @param string $sSql   - SQL da consulta
   * @return Array $aRetDB - Array de dados
   */
  public function buscarInfoDB($sSql) {


    $sResultado = mysql_query($sSql);
    if (!$sResultado) {
      die('Erro ao utilizar m�todo buscarInfoDB: ' . mysql_error());
      return false;
    }

    $this->iLinhas = mysql_num_rows($sResultado);

    while ($aResultado = mysql_fetch_row($sResultado)) {
      $this->RETDB[] = $aResultado;
      $aRetDB = $aResultado;
    }
    return $aRetDB;
  }
   

  /* wTools::buscarParametro
   *
   * Busca valores salvos na tabela de par�metros
   * @date 19/11/2011
   * @param  string $mParametros - Array ou String contendo os par�metros solicitados
   * @return $sExt               - Extensao do arquivo
   */
  public function buscarParametro($mParametros) {

    $sParametros = $this->montarIN($mParametros);
    
    $sTabela = 'tcg_parametros';

    $sQuery = "SELECT NM_TITULO,
                      TX_VALOR
                 FROM ".$sTabela. "
                WHERE nm_titulo IN (".$sParametros.")";
 
    $sResultado = mysql_query($sQuery);
    if (!$sResultado) {
      die('Erro ao buscar par�metro de configura��o');
      return false;
    }
    while ($aResultado = mysql_fetch_array($sResultado)) {
      $this->aParametros[$aResultado['NM_TITULO']][] = $aResultado['TX_VALOR'];
    }
    return true;
  }

  /* wTools::montarIN
   *
   * Busca valores salvos na tabela de par�metros
   * @date 19/11/2011
   * @param  string $mParametros - Array ou String contendo os par�metros solicitados
   * @return $sValorMontado               - Extensao do arquivo
   */
  public function montarIN($mValores) {
    if(!is_array($mValores)) {
      $aValores = explode(',', $mValores);
    } else {
      $aValores = $mValores;
    }
    $sValorMontado = '';
    foreach ($aValores as $sValor) {
      $sValorMontado .= "'".$sValor."', ";
    }
    $sValorMontado = substr($sValorMontado,  0, -2);
       
    return $sValorMontado;
  }

  /* wTools::resumirTexto
   *
   * Trata um conte�do deixando somente uma quantidade de texto para ser usada
   * como resumo.
   * @date 04/12/2011
   * @param  string $sParagrafo  - Texto a ser tratado
   * @param  integer $iQntLetras - Quantidade de caracteres que ser�o exibidos
   * @return string $sResumo     - Texto tratado no tamanho definido
   */
	public function resumirTexto($sParagrafo, $iQntLetras){
		$sResumo = substr($sParagrafo , 0 , $iQntLetras);
		$iCorta  = strrpos($sResumo, " ");
		$resumo  = substr($sParagrafo , 0, $iCorta);

		return $sResumo;
	}

  /* lunacomUtil::caixaItensRelacionados
   *
   * Monta uma caixa que apresenta uma lista de links relacionados a um assunto
   * para que o usu�rio continue navegando
   * @date  12/02/2012
   * @param string $sSql   - Script para busca de dados
   * @param string $sClass - Estilo que ser� apresentada a lista
   * @return true
   */
  public function caixaItensRelacionados($sSql, $sClass, $sDesc = 'Itens Relacionados:'  ) {

    $this->buscarInfoDB($sSql); ?>
    <div class="<?php echo $sClass; ?>">
      <h3><?php echo $sDesc; ?></h3>
      <ul>
        <?php
        foreach ($this->RETDB as $aDados) {
          ?>
          <li><?php $this->montaLink($aDados[0], $aDados[1]);?></li>
          <?php
        }?>
      </ul>
    </div>

    <?php
    return true;
  }
}
?>