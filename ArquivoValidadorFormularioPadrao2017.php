<?php
include_once 'ArquivosInterface.php';
class ArquivoValidadorFormularioPadrao2017 implements ArquivosInterface{
  private $oNovaClasse;
  public function __construct(novaClasse $oNovaClasse) {
    $this->oNovaClasse = $oNovaClasse;
  }

  public function gerar() {
    $sConteudo = $this->montarDados();
    $this->oNovaClasse->fecharArquivo('Validador'.$this->oNovaClasse->getNomePadronizado().'.php',$sConteudo);
  }
  
  private function montarDados() {
    $sConteudo = '<?php
'.$this->oNovaClasse->sCabecalho.'
  include_once \'modulosPHP/excecao/ExceptionValidador.php\';
  class Validador'.$this->oNovaClasse->getNomePadronizado().' {'.LN;
    
    $sConteudo .= $this->gerarConstruct()."\n";
    $sConteudo .= $this->gerarValidar()."\n";
    $sConteudo .= IND02."}";
    return $sConteudo;
  }
    
  private function gerarConstruct() {
    $sConteudo = IND04.'public $oUtil;';
    $sConteudo = IND04.'public function __construct() {';
    $sConteudo = IND06.'$this->oUtil = new wTools();';
    $sConteudo = IND04.'}';
  }

  private function gerarValidar() {
    $sConteudo = IND04.'public function dados ( Modelo'.$this->oNovaClasse->getNomePadronizado().' $oModelo ) {'."\n";
    $sConteudo .= IND06.'$aValidar = array ( ';

    for ($i = 1; $i < $this->oNovaClasse->iLinhasTabela; $i++) {
      $iOrdemDaValidacao = $i * 10;
      $sCampo = $this->oNovaClasse->NM_CAMPO[$i];

      $sNomeDoCampo = $this->oNovaClasse->gerarCamelCase($sCampo);
      $sIdentacao            = $this->oNovaClasse->calculaIndentacao($sNomeDoCampo);
//      $sIdentacaoTipoDeCampo = $this->oNovaClasse->calculaIndentacao($sNomeDoCampo, 'TP_CAMPO');

      $sConteudo .= $iOrdemDaValidacao.' => array(\'';      
      $sConteudo .= ucfirst($sNomeDoCampo).'\''.$sIdentacao;

      $sConteudo .= ', $oModelo->'.$sNomeDoCampo.$sIdentacao;
      $sConteudo .= ', \''.$this->oNovaClasse->TP_CAMPO[$i].'\'';
      $sConteudo .= ', true';
      $sConteudo .= $this->oNovaClasse->TP_CAMPO_TAM[$i] != '' ? ', array'.$this->oNovaClasse->TP_CAMPO_TAM[$i] : '' ;
      
      $sConteudo .= '),';
      
      $sConteudo .= ($i != $this->oNovaClasse->iLinhasTabela - 1) ? LN.IND26 : ''; 
    }

    $sConteudo .= LN.IND24.');';
    $sConteudo .= LN.IND06.'return $aValidar;';
    $sConteudo .= LN.IND04."}";
    return $sConteudo;
  }
}