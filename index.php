<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <title>Gerador de Classes Lunacom </title>
      <script src="modulosJS/jquery.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(document).ready(function(){
          $('#CMPbancoSelecionado').change(function(){
            selecionarTabelasBD($('#CMPbancoSelecionado option:selected').text());
          })

        });
        function selecionarTabelasBD(sBancoSelecionado) {
          $.ajax({
            type: "POST",
            data: "sAcao=selecionarTabelasBD&sBancoSelecionado="+sBancoSelecionado,
            url: "tratarAjax.php",
            context: document.body,
            async: true,
            success: function(html){
              $("#conteinerCMPnomeClasse").html(html);
            }
          });
          return false;
        }
      </script>


  </head>
  <body>
    <?php
      if (!isset($_POST['CMPmostrarWarningsPHP'])) {
        error_reporting(E_ERROR | E_PARSE);
      }

      include 'class.gerador.php';
      include 'class.novaClasseMysql.php';
      include 'class.novaClasseAdo.php';
      include 'class.novaClasseMvc01.php';
      include_once 'class.wTools.php';
      $oClasse = new novaClasse();
      $oUtil   = new wTools();
      
      if(isset($_POST['FRMgerar'])) {
        //Gerar todas classes para um banco de dados
        if (isset($_POST['CMPtodas'])) {
          $aTabelasBD = $oClasse->buscarTabelasBD($_POST['CMPbancoSelecionado']);
        } else {
          $aTabelasBD = array($_POST['CMPnomeClasse']);
        }
        
        $bCriarGettersSetters = isset($_POST['CMPgetSet']);

        $aClasses = array ('FIXO' => 'novaClasseMysql',
                            'ADO' => 'novaClasseAdo',
                         'MVC_01' => 'novaClasseMvc01');

        foreach ($aTabelasBD as $sTabelaSelacionada) {

          $oClasse = new $aClasses[$_POST['CMPconexao']]();
          
          $oClasse->bCriarGettersSetters = $bCriarGettersSetters;
          
          $oClasse->sBancoSelecionado = $_POST['CMPbancoSelecionado'];
          $oClasse->sProjeto          = $_POST['CMPprojeto'];
          $oClasse->sAutor            = $_POST['CMPautor'];
          $oClasse->sAutorEmail       = $_POST['CMPemail'];
          $oClasse->sDireitos         = $_POST['CMPdireitos'];
          $oClasse->sPrefixo          = $_POST['CMPprefixo'];
          $oClasse->sConexao          = $_POST['CMPconexao'];
          $oClasse->sNomeClasse       = $sTabelaSelacionada;

          $oClasse->executa();
        }
      } else {
        $_POST['CMPgerarDao']        = 'S';
        $_POST['CMPgerarControle']   = 'S';
        $_POST['CMPgerarVisao']      = 'S';
        $_POST['CMPgerarNegocio']    = 'S';
        $_POST['CMPgerarModelo']     = 'S';
      }

    ?>
    <h2>Gerador de Classes padr�o Lunacom</h2>
    <p>Inserir os dados nos respectivos campos para gerar uma nova classe.</p>
    <p>
      A nova classe ficar� salva dentro da pasta "Arquivos Gerados". <br />
      Observa��es: <br />    
    </p>
      <ul>
        <li>IDs das tabelas devem por padr�o ser o primeiro campo</li>
        <li>O nome da classe gerada ser� igual ao nome da tabela</li>
        <li>Ao gerar uma nova classe, selecionar corretamente o banco de dados da tabela</li>
        <li>Eventuais altera��es nos c�digos gerados podem ser requeridas</li>
      </ul>
    <form name="FRMdadosNovaClasse" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
      <table>
        <tr>
          <td>Selecionar Banco</td>
          <td>
            <?php
              $sBancoEmUso = (isset($_POST['CMPbancoSelecionado'])) ? $_POST['CMPbancoSelecionado'] : '';
              $oClasse->listaBancos($sBancoEmUso);
            ?>
          </td>
        </tr>
        <tr>
          <td>Projeto:</td>
          <td><input type="text" name="CMPprojeto"    value="<?php echo (isset($_POST['CMPprojeto']) ? $_POST['CMPprojeto'] : 'Site Lunacom'); ?>" /></td>
        </tr>
        <tr>
          <td>Autor:</td>
          <td><input type="text" name="CMPautor"      value="<?php echo isset($_POST['CMPautor']) ? $_POST['CMPautor'] : 'Alex Lunardelli'; ?>" /></td>
        </tr>
        <tr>
          <td>Email:</td>
          <td><input type="text" name="CMPemail"      value="<?php echo isset($_POST['CMPemail']) ? $_POST['CMPemail'] : 'alex@lunacom.com.br'; ?>" /></td>
        </tr>
        <tr>
          <td>Direitos:</td>
          <td><input type="text" name="CMPdireitos"   value="<?php echo isset($_POST['CMPdireitos']) ? $_POST['CMPdireitos'] : 'Lunacom marketing Digital'; ?>" /></td>
        </tr>
        <tr>
          <td>Todas tabelas:</td>
          <td><input type="checkbox" name="CMPtodas" value="todas"  /></td>
        </tr>
        <tr>
          <td>Criar getters e setters:</td>
          <td><input type="checkbox" name="CMPgetSet" value="1"  /></td>
        </tr>
        <tr>
          <td>Prefixo do arquivo:</td>
          <td>
            <input type="radio" name="CMPprefixo" value="DAO"   <?php echo (!isset($_POST['CMPprefixo']) || $_POST['CMPprefixo'] == 'DAO') ? 'checked="checked"' : ''; ?> />Dao
            <input type="radio" name="CMPprefixo" value="CLASS" <?php echo (isset($_POST['CMPprefixo']) && $_POST['CMPprefixo'] == 'CLASS') ? 'checked="checked"' : ''; ?> />Class
          </td>
        </tr>
        <tr>
          <td>Arquitetura:</td>
          <td>
            <input type="radio" name="CMPconexao" value="MVC_01"<?php echo (!isset($_POST['CMPconexao']) || $_POST['CMPconexao'] == 'MVC_01') ? 'checked="checked"' : ''; ?> />MVC 01
            <input type="radio" name="CMPconexao" value="ADO"  <?php echo (isset($_POST['CMPconexao']) && $_POST['CMPconexao'] == 'ADO') ? 'checked="checked"' : ''; ?> />ADO (com classe de conex�o)
            <input type="radio" name="CMPconexao" value="FIXO" <?php echo (isset($_POST['CMPconexao']) && $_POST['CMPconexao'] == 'FIXO') ? 'checked="checked"' : ''; ?> />Fixo (mysql com arquivo conecta.php)
          </td>
        </tr>
        <tr>
          <td>Mostrar Warnings PHP:</td>
          <td>
            <input type="checkbox" name="CMPmostrarWarningsPHP" value="S"  <?php echo (isset($_POST['CMPmostrarWarningsPHP'])) ? 'checked="checked"' : ''; ?> />
          </td>
        </tr>
        <tr>
          <td>Gerar Controle:</td>
          <td>
            <input type="checkbox" name="CMPgerarControle" value="S"  <?php echo (isset($_POST['CMPgerarControle'])) ? 'checked="checked"' : ''; ?> />
          </td>
        </tr>
        <tr>
          <td>Gerar Vis�o:</td>
          <td>
            <input type="checkbox" name="CMPgerarVisao" value="S"  <?php echo (isset($_POST['CMPgerarVisao'])) ? 'checked="checked"' : ''; ?> />
          </td>
        </tr>
        <tr>
          <td>Gerar Neg�cio:</td>
          <td>
            <input type="checkbox" name="CMPgerarNegocio" value="S"  <?php echo (isset($_POST['CMPgerarNegocio'])) ? 'checked="checked"' : ''; ?> />
          </td>
        </tr>
        <tr>
          <td>Gerar Modelo:</td>
          <td>
            <input type="checkbox" name="CMPgerarModelo" value="S"  <?php echo (isset($_POST['CMPgerarModelo'])) ? 'checked="checked"' : ''; ?> />
          </td>
        </tr>
        <tr>
          <td>Nome da Classe:</td>
          <td id="conteinerCMPnomeClasse">
            <?php
              if (isset ($_POST['CMPbancoSelecionado'])) {
                $oClasse->listarTabelasBD($_POST['CMPbancoSelecionado']);
              } else {
            ?>
            <select id="CMPnomeClasse" name="CMPnomeClasse">
              <option>Selecione um banco de dados</option>
            </select>
            <?php
              }
            ?>
          </td>
        </tr>
        <tr>
          <td><div id="teste"></div></td>
          <td><input type="submit" name="FRMgerar"    value="Gerar Classe" /></td>
        </tr>
      </table>
    </form>
  </body>
</html>
