<?php

interface interfaceTipoGerador {
  public function criaCabecalho();
  public function criaListar();
  public function criaSalvar();
  public function criaInserir();
  public function criaRemover();
  public function criaEditar();
  public function criaSetters();
  public function criaGetters();
  public function criaInicializaAtributos();
}
