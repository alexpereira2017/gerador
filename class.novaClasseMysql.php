<?php //
/**
 * Nova classe para separar diferentes arquivos gerados.
 * Esta classe gera arquivos utilizando banco de dados mysql, fun��es PHP nativas
 * e conex�o com banco utilizando um arquivo chamado "conecta.php"
 *
 * @author Alex Lunardelli
 * @date   30/09/2014
 */
include_once 'class.gerador.php';
include_once 'interfaceTipoGerador.php';

class novaClasseMysql extends novaClasse implements interfaceTipoGerador{

  public function __construct() {
    parent::__construct();
  }

  public function criaCabecalho() {

    $this->sCabecalho = '
<?php
  /**
   * Descricao
   *
   * @package    '.$this->sProjeto.'
   * @author     '.$this->sAutor.' <'.$this->sAutorEmail.'>
   * @copyright  '.$this->sDireitos.'
   * @date       '.$this->sDataAtual.'
   **/

  class '.$this->sNomeClasse.' {
  ';

    foreach ($this->NM_CAMPO as $sNome) {
      $this->sCabecalho .='
    public    $'.$sNome.';';
    }

    foreach ($this->NM_CAMPO as $sNome) {
      $this->sCabecalho .='
    public    $'.strtoupper($sNome).$this->calculaIndentacao($sNome).' = array();';
    }
    $this->sCabecalho .='
    public    $iCdMsg;
    public    $sMsg;
    public    $aMsg;
    public    $sErro;
    public    $sBackpage;
    protected $DB_LINK;
    protected $oUtil;

    public function __construct() {
      include \'conecta.php\';
      $this->DB_LINK = $link;
      $this->oUtil   = new wTools();
    }
';
  }
  
  public function criaListar() {

    $CFGadcionalListagem = $this->buscaConfig();

    $sConteudo  = '
      
    public function listar($sFiltro = \'\') {';
    $sConteudo .= '
      $sQuery = \'SELECT '.$this->NM_CAMPO[0].','.LN;
    
    for ($i = 1; $i < $this->iLinhasTabela; $i++) {
      if($this->TP_CAMPO[$i] == 'date') {
        $sConteudo .=           
          IND24.'date_format('.$this->NM_CAMPO[$i].', "%d/%m/%Y") AS '.$this->NM_CAMPO[$i] . ($this->iLinhasTabela != ($i + 1) ? ', '.LN : ' '.LN);
      } elseif ($this->TP_CAMPO[$i] == 'time') {
        $sConteudo .=
          IND24.'date_format('.$this->NM_CAMPO[$i].', "%H:%i") AS '.$this->NM_CAMPO[$i] . ($this->iLinhasTabela != ($i + 1) ? ', '.LN : ' '.LN);
      } else {
        $sConteudo .=
          IND24 . $this->NM_CAMPO[$i] . ($this->iLinhasTabela != ($i + 1) ? ', '.LN : ' '.LN);
      }
    }
        
    $sConteudo .= IND19.'FROM '.$this->sNomeClasse;
    $sConteudo .= LN.IND19.'\'.$sFiltro;';

    $sConteudo .= '
      $sResultado = mysql_query($sQuery, $this->DB_LINK);

      if (!$sResultado) {
        die(\'Erro ao criar a listagem: \' . mysql_error());
        return false;
      }

      //$this->iLinhas'.ucfirst($this->sNomeClasse).' = mysql_num_rows($sResultado);
      $this->iLinhas = mysql_num_rows($sResultado);
        
      while ($aResultado = mysql_fetch_array($sResultado)) { ';
      foreach ($this->NM_CAMPO as $sNome) {
        $sConteudo .= LN.IND08.'$this->'.strtoupper($sNome).'[]'.$this->calculaIndentacao($sNome).' = $aResultado[\''.$sNome.'\']; ';
      }
      $sConteudo .= LN.IND06.'}';
      $sConteudo .= LN.IND04.'}'.LN;
      
      
    $this->sListagem = $sConteudo;
  }
  
  public function criaSalvar() {
    $sNomeTabela = $this->retirarSigla($this->sNomeClasse);
    $sConteudo = LN.IND04.'public function salvar() {'.LN;

    $sConteudo .= '
      try {
        $aValidar = array ( ';

    for ($i = 1; $i < $this->iLinhasTabela; $i++) {
      $sNomeCampo = $this->retirarSigla($this->NM_CAMPO[$i]);

      $sConteudo .= $i.' => array(\''.ucfirst($sNomeCampo).'\' , $_POST[\'CMP'.$sNomeTabela.'-'.$sNomeCampo.'\'], \''.$this->TP_CAMPO[$i].'\', true),'.LN.IND28;
    }
                            

    $sConteudo .= ');

        // Valida��o de preenchimento
        if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
          $this->aMsg = $this->oUtil->aMsg;
          throw new excecoes(25);
        }

        // Editar conte�do
        if ($_POST[\'sAcao\'] == \'editar\') {
          $this->editar($this->ID[0]);

        } elseif ($_POST[\'sAcao\'] == \'inserir\') {
          $this->inserir();
          $this->oUtil->redirFRM($this->sBackpage, $this->aMsg);
          header(\'location:\'.$this->sBackpage);
          exit;
        } else {

          // Tipo de a��o n�o � v�lido
          throw new excecoes(20, $this->oUtil->anti_sql_injection($_POST[\'CMPpgAtual\']));
        }

      } catch (excecoes $e) {
        $e->bReturnMsg = false;
        $e->getErrorByCode();
        if (is_array($e->aMsg)) {
          $this->aMsg = $e->aMsg;
        }
        return false;
      }
';

    $sConteudo .= IND06.'return true;'.LN.IND04.'}';
    $this->sSalvar = $sConteudo.LN;
  }
  
  public function criaInserir() {
    $sConteudo = LN.IND04.'public function inserir() {';

    $sConteudo .= '
      $sQuery = "INSERT INTO '.$this->sNomeClasse.'('.LN.IND29;
    for ($i = 1; $i < $this->iLinhasTabela; $i++) {
      $sConteudo .= strtoupper($this->NM_CAMPO[$i]) . ($this->iLinhasTabela != ($i + 1) ? ', '.LN.IND29 : ' '.LN);
    }
    $sConteudo .= ')'.LN;
    $sConteudo .= IND06.'VALUES('.LN_INSERT;
    for ($i = 0; $i < $this->iLinhasTabela; $i++) {
      
      // Se � chave prim�ria n�o � inserido via PHP
      if($this->CD_KEY[$i] != 'PRI') {
        $sConteudo .= '\'".$this->'.strtoupper($this->NM_CAMPO[$i]).'[0]'.($this->iLinhasTabela != ($i + 1) ? '."\', '.LN_INSERT : '."\' ');
      }
    }
    $sConteudo .= '
    )";'.LN;
    $sConteudo .= IND06.'$sResultado = mysql_query($sQuery, $this->DB_LINK);

      if (!$sResultado) {
        $this->iCdMsg = 1;
        $this->sMsg  = \'Ocorreu um erro ao salvar o registro.\';
        $this->sErro = mysql_error();
    	$this->sResultado = \'erro\';
        $bSucesso = false;

      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = \'O registro foi adicionado com sucesso!\';
        $this->sResultado = \'sucesso\';
        $bSucesso = true;
      }

      // Monta array com mensagem de retorno
      $this->aMsg = array(\'iCdMsg\' => $this->iCdMsg,
                            \'sMsg\' => $this->sMsg,
                      \'sResultado\' => $this->sResultado );
      return $bSucesso;'.LN.IND04.'}';
     $this->sInserir = $sConteudo.LN;
  }
  public function criaRemover() {

    $sConteudo  = LN.IND04.'public function remover($sFiltro) {';
    $sConteudo .= '
      $sQuery = "DELETE FROM '.$this->sNomeClasse.' ".$sFiltro;'.LN;

      $sConteudo .= IND06.'$sResultado = mysql_query($sQuery, $this->DB_LINK);

      if (!$sResultado) {
        $this->iCdMsg = 1;
        $this->sMsg  = \'Ocorreu um erro ao remover o registro.\';
        $this->sErro = mysql_error();
        $this->sResultado = \'erro\';
        $bSucesso = false;

      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = \'O registro foi removido com sucesso!\';
        $this->sResultado = \'sucesso\';
        $bSucesso = true;
      }

    // Monta array com mensagem de retorno
    $this->aMsg = array(\'iCdMsg\' => $this->iCdMsg,
                          \'sMsg\' => $this->sMsg,
                    \'sResultado\' => $this->sResultado );
    return $bSucesso;'.LN.IND02;

    $this->sRemover = $sConteudo.'}'.LN;

  }
  
  public function criaEditar() {
    $sConteudo  = LN.IND04.'public function editar($iId = \'\') {';
    $sConteudo .= LN.IND06.'$sQuery = "UPDATE '.$this->sNomeClasse;
    $sConteudo .= LN.IND08.'SET';
    for ($i = 1; $i < $this->iLinhasTabela; $i++) {
      $sConteudo .= LN.IND10.$this->NM_CAMPO[$i].$this->calculaIndentacao($this->NM_CAMPO[$i]).' = \'".$this->'.strtoupper($this->NM_CAMPO[$i]).'[0]'.($this->iLinhasTabela != ($i + 1) ? '."\', ' : '."\' ');
    }
    $sConteudo .= LN.IND10.'WHERE '.$this->NM_CAMPO[0].' = ".$iId;';  //id = '".$this->id[0]."',
    $sConteudo .= '
      $sResultado = mysql_query($sQuery, $this->DB_LINK);

      if (!$sResultado) {
        $this->iCdMsg = 1;
        $this->sMsg  = \'Ocorreu um erro ao salvar o registro.\';
        $this->sErro = mysql_error();
    	$this->sResultado = \'erro\';
        $bSucesso = false;

    	} else {
        $this->iCdMsg = 0;
        $this->sMsg  = \'O registro foi editado com sucesso!\';
        $this->sResultado = \'sucesso\';
        $bSucesso = true;
      }
    // Monta array com mensagem de retorno
    $this->aMsg = array(\'iCdMsg\' => $this->iCdMsg,
                          \'sMsg\' => $this->sMsg,
                    \'sResultado\' => $this->sResultado );
    return $bSucesso;
     ';
    $sConteudo .= LN.IND04.'}';
    $this->sEditar = $sConteudo;
  }
  
  public function criaSetters() {
    $sConteudo = LN.LN;
    for ($i = 1; $i < $this->iLinhasTabela; $i++) {
      $sConteudo .= IND04.'public function set'.ucfirst($this->NM_CAMPO[$i]).'($'.$this->NM_CAMPO[$i].') {'.LN;

      if($this->TP_CAMPO[$i] == 'date') {
        $sConteudo .= IND06.'$this->'.$this->NM_CAMPO[$i].' = $'.$this->NM_CAMPO[$i].';'.LN;
      } elseif ($this->TP_CAMPO[$i] == 'time') {
        $sConteudo .= IND06.'$this->'.$this->NM_CAMPO[$i].' = $'.$this->NM_CAMPO[$i].';'.LN;
      } else {
        $sConteudo .= IND06.'$this->'.$this->NM_CAMPO[$i].' = $'.$this->NM_CAMPO[$i].';'.LN;
      }
      $sConteudo .= IND04.'}'.LN.LN;
    }
    
    $this->sSetters = $sConteudo;
  }

  public function criaGetters() {
    $sConteudo = LN.LN;
    for ($i = 1; $i < $this->iLinhasTabela; $i++) {
      $sConteudo .= IND04.'public function get'.ucfirst($this->NM_CAMPO[$i]).'($'.$this->NM_CAMPO[$i].') {'.LN;

      if($this->TP_CAMPO[$i] == 'date') {
        $sConteudo .= IND06.'return $this->'.$this->NM_CAMPO[$i].';'.LN;
      } elseif ($this->TP_CAMPO[$i] == 'time') {
        $sConteudo .= IND06.'return $this->'.$this->NM_CAMPO[$i].';'.LN;
      } else {
        $sConteudo .= IND06.'return $this->'.$this->NM_CAMPO[$i].';'.LN;
      }
      $sConteudo .= IND04.'}'.LN.LN;
    }

    $this->sGetters = $sConteudo;
  }
  

  public function criaInicializaAtributos() {
    $sNomeTabela = $this->retirarSigla($this->sNomeClasse);
    $sConteudo  = '

    public function inicializaAtributos() {'.LN.LN.IND06;

    for ($i = 0; $i < $this->iLinhasTabela; $i++) {

      $sIdentacao = $this->calculaIndentacao($this->NM_CAMPO[$i]);
      $sNomeCampo = $this->retirarSigla($this->NM_CAMPO[$i]);
      $sConteudo .= '$this->'.strtoupper($this->NM_CAMPO[$i]).'[0]'.$sIdentacao.' = (isset ($_POST[\'CMP'.$sNomeTabela.'-'.($this->retirarSigla($this->NM_CAMPO[$i])).'\']) '.$sIdentacao.'? $_POST[\'CMP'.$sNomeTabela.'-'.($this->retirarSigla($this->NM_CAMPO[$i])).'\'] '.$sIdentacao.': \'\');'.LN.IND06;
    }
    $sConteudo .= LN.IND04.'}';

    $this->sInicializaAtributos = $sConteudo;
  }

}
