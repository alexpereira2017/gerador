<?php
include_once 'ArquivosInterface.php';
class ArquivoControlePadrao2017 implements ArquivosInterface{
  private $oNovaClasse;
  public function __construct(novaClasse $oNovaClasse) {
    $this->oNovaClasse = $oNovaClasse;
  }

  public function gerar() {
    $sConteudo = $this->montarDados();
    $this->oNovaClasse->fecharArquivo('Controle'.$this->oNovaClasse->getNomePadronizado().'.php',$sConteudo);
  }
  
  private function montarDados() {
$sConteudo = '<?php
include_once \'modulosPHP/validador/Validador'.$this->oNovaClasse->getNomePadronizado().'.php\';
class Controle'.$this->oNovaClasse->getNomePadronizado().' {
  
  private $oNegocio;
  private $oModelo;
  private $oVisao;
  private $oValidador;


  public function __construct() {
    
    $this->oModelo = new Modelo'.$this->oNovaClasse->getNomePadronizado().'();
    $this->oNegocio = new Negocio'.$this->oNovaClasse->getNomePadronizado().'();
    
    if (isset($_POST)) {
      if (isset($_POST[\'sAcao\'])) {
        if ($_POST[\'sAcao\'] == \'salvar\') {

          try {
            $this->oValidador = new Validador'.$this->oNovaClasse->getNomePadronizado().'();
            $this->oModelo->carregarDadosAposPost($_POST);
            $this->oValidador->validar($this->oModelo);

            $this->oNegocio->salvar($this->oModelo);
            
            $this->oModelo = new Modelo'.$this->oNovaClasse->getNomePadronizado().'();
            $this->oModelo->mAcaoResultado = 0;
            $this->oModelo->sAcaoMsg       = \'Novo cadastro foi realizado com sucesso!\';
            
          } catch (exception_validacao $ex) {
            $this->oModelo->mAcaoResultado = 2;
            $this->oModelo->sAcaoMsg       = $this->oValidador->aMsg[\'sMsg\'];
          }          
        }
      }
    }

    $this->oVisao  = new Visao'.$this->oNovaClasse->getNomePadronizado().'($this->oModelo);
  }
  
  public function getVisao() {
    return $this->oVisao;
  }
}

';
  return $sConteudo;
    
  }
}
