<?php

/**
 * Description of class
 *
 * @author Alex Lunardelli
 */

//include 'conecta.php';

define("LN", "
");
define("LN_INSERT", "
              ");
define("IND02", "  ");
define("IND04", "    ");
define("IND06", "      ");
define("IND08", "        ");
define("IND10", "          ");
define("IND12", "            ");
define("IND14", "              ");
define("IND16", "                ");
define("IND17", "                 ");
define("IND18", "                  ");
define("IND19", "                   ");
define("IND20", "                    ");
define("IND22", "                      ");
define("IND24", "                        ");
define("IND26", "                          ");
define("IND27", "                           ");
define("IND28", "                            ");
define("IND29", "                             ");
define("IND40", "                                        ");


class novaClasse {

  public $sBancoSelecionado = '';
  public $sProjeto = '';
  public $sAutor = 'Alex';
  public $sAutorEmail = 'alex@lunacom.com.br';
  public $sDireitos = '';
  public $sNomeClasse = '';
  public $sOrdenarBuscasPor;
  public $sDataAtual;
  public $sPrefixo;
  public $sErro;
  public $iMaiorCampo = 0;
  public $iLinhasTabela;
  
  public $sCabecalho;
  public $sListagem;
  public $sInserir;
  public $sRemover;
  public $sAtualizar;
  public $sSalvar;


  public $sGetters = '';
  public $sSetters = '';
  public $bCriarGettersSetters;


  public function getNomeTabela() {
    return $this->retirarSigla($this->sNomeClasse);
  }

  public function __construct() {
    
    $this->sDataAtual = date('d-m-Y');
    $this->conecta();
  }

  public function conecta() {
    // $this->sLink = mysql_connect('localhost', 'root', 'mylunacom');
    $this->sLink = mysql_connect('localhost:7001', 'root', '123456');
    
    if (!$this->sLink) {
      die('N�o foi poss�vel conectar: ' . mysql_error());
    }       
    return true;
  }

  public function selecionaBD() {
    $sDBslected = $this->sBancoSelecionado;
    mysql_select_db($sDBslected);
  }

  public function buscarTabelasBD($sDatabase) {
    $sDatabase = trim($sDatabase);
    $this->oTableList = mysql_list_tables($sDatabase);
    while ($row = mysql_fetch_array($this->oTableList)) {
      $aTabelas[] = $row[0];
    }
    return $aTabelas;
  }

  public function listarTabelasBD($sDatabase) {
    $sDatabase = trim($sDatabase);
    $this->oTableList = mysql_list_tables($sDatabase);
    ?>
      <select id="CMPnomeClasse" name="CMPnomeClasse">
        <?php
          while ($row = mysql_fetch_array($this->oTableList)) {
            echo '<option '.((isset($_POST['CMPnomeClasse']) && $_POST['CMPnomeClasse'] === $row[0]) ? 'selected="selected"' : '') .'>'.$row[0].'</option>';
          }?>
      </select>
    <?php
  }

  public function listaBancos($sBancoEmUso = '') {
    $this->oDbList = mysql_list_dbs($this->sLink);?>

    <select id="CMPbancoSelecionado" name="CMPbancoSelecionado">
    <?php
      while ($row = mysql_fetch_object($this->oDbList)) { ?>
        <option <?php echo $sBancoEmUso == $row->Database ? 'selected="selected"' : ''?>>
          <?php
            echo $row->Database;
          ?>
        </option>
    <?php
    } ?>
    </select>
    <?php
  }

  public function buscaDados() {
    // Busca o Conteudo dos campos da tabela selecionada pelo usu�rio (nome, tipo etc)
    $sQuery = 'SHOW COLUMNS FROM '.$this->sNomeClasse;
    $sResultado = mysql_query($sQuery);

    if(mysql_errno($this->sLink)) {
      $this->sErro = 'Erro :  '.mysql_errno($this->sLink) . ": " . mysql_error($this->sLink);
      $this->iLinhasTabela = 0;
    } else {
      $this->iLinhasTabela = mysql_num_rows($sResultado);
      while ($aResultado = mysql_fetch_array($sResultado)) {
	
	// Trata os tipo que possuem limita�ao de tamanho ex: decimal(18,2)
	if (strpos($aResultado['Type'],'(') !== false) {
	  $aRet = explode('(', $aResultado['Type']);
	  $aResultado['Type'] = $aRet[0];
	  $this->TP_CAMPO_TAM[] = '('.$aRet[1];
	} else {
	  $this->TP_CAMPO_TAM[] = '';
	}
	
        $this->NM_CAMPO[]       = $aResultado['Field'];
        $this->TP_CAMPO[]       = $aResultado['Type'];
	$this->TX_DEFAULT[]     = $aResultado['Default'];
        $this->OP_NULL[]        = $aResultado['Null'];
        $this->CD_KEY[]         = $aResultado['Key'];
        $this->TX_DEFAULT[]     = $aResultado['Default'];
//        $iCampoNome             = strlen($aResultado['Field']);
//        $this->iMaiorNome       = $iCampoNome > $this->iMaiorNome ? $iCampoNome : $this->iMaiorNome;
      }
    }
  }


  protected function buscaConfig() {
    $CFGadcionalListagem = array( 'date' => '',
                                  'time' => ''
                                );
  }


  /* novaClasse::retirarSigla
   *
   * Para retirar as siglas do nome de tabelas e campos do banco de dados.
   *
   * @date 07/10/2012
   * @param  string $sNome - String com o nome que tera a sigla retirada
   * @return string        - Nome preparado
   */
  public function retirarSigla($sNome) {
    $aNome = explode('_', $sNome);

    // Se o array possui somente um campo, provavelmente seja o ID da tabela
    if (count($aNome) == 1) {
      return $aNome[0];
    }
    unset ($aNome[0]);
    return implode('-', $aNome);
  }

  /* novaClasse::criarFormPadrao
   *
   * Base para criar formul�rio para a classe
   *
   * @date 07/10/2012
   * @param  string $sNome - String com o nome que tera a sigla retirada
   * @return string $sStr - String pronta
   */
  public function criarFormPadrao() {

    // Cria a base do nome, tirando a sigla da tabela
    $sNomeTabela = $this->retirarSigla($this->sNomeClasse);
    $sNomeObjeto = ucfirst($sNomeTabela);

    $sConteudo  = IND06;
    $sConteudo .= '/**********************************************************************'.LN.IND06;
    $sConteudo .= '  Cabe�alho de tratamento do formul�rio'.LN.IND06;
    $sConteudo .= '**********************************************************************/'.LN.IND06;

    
    
    $sConteudo .= '

          $o'.ucfirst($sNomeTabela).' = new '.$this->sNomeClasse.'();

          if (isset($_POST[\'sAcao\'])) {

            // Persistencia de dados na tela
            $o'.ucfirst($sNomeTabela).'->inicializaAtributos();

            //Anti SQL injection
            foreach ($_POST as $sNome => $mValor) {
              if (!is_array($mValor)) {
                $_POST[$sNome] = $oSite->anti_sql_injection($mValor);
              }
            }

            // Manipula��o dos dados
            $oMan'.ucfirst($sNomeTabela).' = new '.$this->sNomeClasse.'();
            $oMan'.ucfirst($sNomeTabela).'->inicializaAtributos();

            /*
             Campos com tratamento adicional
            */

            try {

              $oMan'.ucfirst($sNomeTabela).'->sBackpage = $CFGaPgAtual[$sPgAtual][\'backPage\'];
              $oMan'.ucfirst($sNomeTabela).'->salvar();
              $aMsg = $oMan'.ucfirst($sNomeTabela).'->aMsg;

            } catch (excecoes $e) {
              $aMsg = $e->aMsg;
            }

          } else {
            try {
              if (isset ($_REQUEST[\'n\'])) {

                if (!is_numeric($_REQUEST[\'n\'])) {
                  throw new excecoes(10, $sPgAtual);
                }

                $iId = $oSite->anti_sql_injection($_REQUEST[\'n\']);
                $o'.ucfirst($sNomeTabela).'->listar(\'WHERE id = \'.$iId);

                if ($o'.ucfirst($sNomeTabela).'->iLinhas < 1) {
                  throw new excecoes(15, $sPgAtual);
                }
              } else {
                $o'.ucfirst($sNomeTabela).'->inicializaAtributos();
              }

            } catch (excecoes $e) {

              $e->getErrorByCode();
              header(\'location:\'.$CFGaPgAtual[$sPgAtual][\'backPage\']);
              exit;
            }
          }'.LN.LN.IND06;


    $sConteudo .= '/**********************************************************************'.LN.IND06;
    $sConteudo .= '  HTML gen�rico do formul�rio'.LN.IND06;
    $sConteudo .= '**********************************************************************/'.LN;

    $sConteudo.= LN.IND06;

    $sConteudo.= '<div class="adm-titulo"><?php echo $CFGaPgAtual[$sPgAtual][\'titulo\'];?></div>'.LN.LN.IND06;
    $sConteudo.= '<div id="msg_ret"><?php $oSite->msgRetAlteracoes($aMsg); ?></div>'.LN.LN;

    ob_start();
    ?>
      <form id="FRM<?php echo $sNomeTabela; ?>" name="FRM<?php echo $sNomeTabela; ?>" action="<?php echo '<?php echo $_SERVER[\'PHP_SELF\'].(isset($_GET[\'n\']) ? \'?n=\'.$_GET[\'n\'] : \'\');?> ';?>" method="post">
        <input type="hidden" name="sAcao" value="<?php echo '<?php echo (isset($_GET[\'n\']) ? \'editar\' : \'inserir\'); ?>';?>" />
        <input type="hidden" name="CMP<?php echo $sNomeTabela; ?>-id" value="<?php echo '<?php echo $o'.$sNomeObjeto.'->ID[0]; ?>'; ?>" />
        <table class="tab_lista_registros">
    <?php
    $sConteudo.= ob_get_clean();
    $sConteudo.= LN;

    for ($i = 1; $i < $this->iLinhasTabela; $i++) {
      $sNomeCampo = $this->retirarSigla($this->NM_CAMPO[$i]);

      if ($this->TP_CAMPO[$i] == 'text') {
        $sCampo = '<td class="infovalue">'.LN.IND14;
        $sCampo .= '<?php'.LN.IND16;
        $sCampo .= '$oFCKeditor = new FCKeditor(\'CMP'.$sNomeTabela.'-'.$sNomeCampo.'\');'.LN.IND16;
        $sCampo .= '$oFCKeditor->BasePath = \'../modulosPHP/fckeditor/\' ;'.LN.IND16;
        $sCampo .= '$oFCKeditor->Width = 600 ;'.LN.IND16;
        $sCampo .= '$oFCKeditor->Height = 400 ;'.LN.IND16;
        $sCampo .= '$oFCKeditor->Value = $o'.$sNomeObjeto.'->'.strtoupper($this->NM_CAMPO[$i]).'[0];'.LN.IND16;
        $sCampo .= '$oFCKeditor->Create() ;'.LN.IND14;
        $sCampo .= '?>'.LN.IND12;
        $sCampo .= '</td>'.LN.IND10;
      } else {
        $sCampo = '<td class="infovalue"><input type="text" name="CMP'.$sNomeTabela.'-'.$sNomeCampo.'" value="<?php echo $o'.$sNomeObjeto.'->'.strtoupper($this->NM_CAMPO[$i]).'[0]; ?>" /></td>'.LN.IND10;
      }

      $sConteudo .= IND10;
      $sConteudo .= '<tr>'.LN.IND12;;
      $sConteudo .= '<td class="infoheader">'.ucfirst($this->retirarSigla($this->NM_CAMPO[$i])) .':</td>'.LN.IND12;
      $sConteudo .= $sCampo;
      $sConteudo .= '</tr>'."\n";
    }

    ob_start();
    ?>
          <tr>
            <td colspan="2">
              <input class="bt" type="reset" value="Limpar" />
              <input class="bt" type="submit" value="Salvar" />
            </td>
          </tr>
        </table>
      </form>
    <?php
    $sConteudo .= ob_get_clean();


    // Salva em arquivo
    $sNomeArquivo = 'form.'.$sNomeTabela.'.php';
    if(file_exists($sNomeArquivo)) {
      unlink($sNomeArquivo);
    }

    $sArquivo = fopen('Arquivos Gerados/'.$sNomeArquivo, "w+");

    // Escreve "exemplo de escrita" no bloco1.txt
    $sResult = fwrite($sArquivo, $sConteudo);

    // Fecha o arquivo
    fclose($sArquivo);
  }

  /*  gerador::calculaIndentacao()
   *  Ir� testar o tamanho do nome passado como par�metro com o maior campo da tabela
   *  e retorna o valor correto para identa�ao.   *
   */
  public function calculaIndentacao($sNome, $sCampo = 'NM_CAMPO') {
    
    $this->calcularMaiorCampo($sCampo);
    
    $iTamCampo = strlen($sNome);
    $iTamIndentacao = $this->iMaiorNome - $iTamCampo;
    $sIndentacao = '';
    $sIndentacao = str_pad($sIndentacao, $iTamIndentacao, ' ', STR_PAD_LEFT);
    return $sIndentacao;
  }
  
  private function calcularMaiorCampo($sCampo) {
    $this->iMaiorNome = 0;

    foreach ($this->$sCampo as $mValor) {
      $iCampoNome             = strlen($mValor);
      $this->iMaiorNome       = $iCampoNome > $this->iMaiorNome ? $iCampoNome : $this->iMaiorNome;      
    }
  }

  public function executa() {

    $this->selecionaBD();
    $this->buscaDados();

    if($this->sErro) {
      echo $this->sErro;
    } else {

      $this->criaCabecalho();
      if ($this->bCriarGettersSetters) {
        $this->criaGetters();
        $this->criaSetters();
      }
      $this->criaSalvar();
      $this->criaInserir();
      $this->criaEditar();
      $this->criaListar();
      $this->criaRemover();
      $this->criaInicializaAtributos();

      $sNomeArquivo = strtolower($this->sPrefixo).'.'.$this->sNomeClasse.'.php';

      if(file_exists($sNomeArquivo)) {
        unlink($sNomeArquivo);
      }

      $sArquivo = fopen('Arquivos Gerados/'.$sNomeArquivo, "w+");

      $sConteudo = $this->sCabecalho.$this->sGetters.$this->sSetters.$this->sSalvar.$this->sListagem.$this->sInserir.$this->sRemover.$this->sEditar.$this->sInicializaAtributos.LN.IND02.'}';

      // Escreve "exemplo de escrita" no bloco1.txt
      $sResult = fwrite($sArquivo, $sConteudo);

      // Fecha o arquivo
      fclose($sArquivo);

      $this->criarFormPadrao();
    }
  }
  
  public function fecharArquivo($sNomeArquivo, $sConteudo) {
    $sArquivo = fopen('Arquivos Gerados/'.$sNomeArquivo, "w+");

    // Escreve "exemplo de escrita" no bloco1.txt
    $sResult = fwrite($sArquivo, $sConteudo);

    // Fecha o arquivo
    fclose($sArquivo);
  }

  public function getNomePadronizado() {
    return $this->gerarCamelCase($this->sNomeClasse);
  }
  
  public function gerarCamelCase( $sNome ) {
    $aNome = explode('_', $sNome);
    $sNomePadronizado = '';
    foreach ($aNome as $value) {
      $sNomePadronizado .= ucfirst($value);
    }
    return $sNomePadronizado;
    
  }
  
}
?>
