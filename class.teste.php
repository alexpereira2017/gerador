
          <?php
            /**
             * Descricao
             *
             * @package    
             * @author     Alex <alex@lunacom.com.br>
             * @copyright  
             * @date
             **/

            /*

            INCLUIR OS ARQUIVOS NECESS�RIOS PARA CONEXAO COM BANCO DE DADOS

            if (basename($_SERVER['PHP_SELF']) == 'class.apcmateriais.php' || $_SERVER['PHP_SELF'] == '') {
              require_once('../inc/class.redirecionaParaURL.php');
              redirecionaParaURL('../index.php');
              exit;
            }

            require_once($helpSkin.$sPrefInc.'../inc/LangMan/lmutil.php');
            bindLanguage('Ditech', $idiomaSessao);
            */


            /**
             * Descricao
             *
             * @package    
             * @copyright  
             **/
            class teste {

              /**
               * Conex�o com o banco onde est� a tabela
               * @var object $oDB
               */
              private $oDB;

              /**
               * Fun��es da Util
               * @var object $oUtil
               */
              private $oUtil;

              /**
               * Linhas encontradas pelo filtro do construtor
               * @var intenger $iLinhas
               */
              public $iLinhas = 0;

              /**
               * Armazena o �ltimo erro ocorrido na classe
               * @var string $sErro
               */
              public $sErro = '';

              /**
               * Paginador dos resultados
               * @var object $oPaginador
               */
              public $oPaginador;

              /**
               * Campos da tabela
               */

              // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex              
              public $ID_MATERIAL = array();
              public $NM_MATERIAL = array();
              public $DE_MATERIAL = array();
              // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex

              /**
               * teste::teste() - Construtor da classe
               *
               * @author  Alex <alex@lunacom.com.br>
               * @param   string  $sFiltro Filtro para popular os dados da classe
               * @return  void
               **/
              public function __construct($sFiltro = '', $bPaginar = false, $sOrdem = '') {
                // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex                  
                if (isset($GLOBALS['util'])) {
                  $this->oUtil =& $GLOBALS['util'];
                } else {
                  $this->oUtil =& $GLOBALS['oUtil'];
                }
                $this->oDBAP =& $GLOBALS['DBAP'];
                // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex

                if (!empty($sFiltro)) {
                  return $this->consultar($sFiltro, $bPaginar, $sOrdem);
                }
              }

              /**
               * teste::consultar() - Armazena os dados da tabela na classe
               *
               * Armazena os dados da tabela na classe
               *
               * @author  Alex <alex@lunacom.com.br>
               * @param   string  $sFiltro Filtro ao trazer os dados da tabela
               * @param   boolean $bPaginar Booleano que indica se deve paginar os resultados
               * @param   string  $sOrdem Campo que ser� ordenado na cl�usula SQL
               * @return  boolean Se a consulta retornou com ou sem erro
               **/
              public function consultar($sFiltro, $bPaginar = false, $sOrdem = '') {

                $sSQL = 'SELECT apcmateriais.id_material,
                                apcmateriais.nm_material,
                                apcmateriais.de_material
                           ';

                $sWhere = 'FROM apcmateriais
                          WHERE (' . $sFiltro . ')';

                $sSQL .= $sWhere;

                if(!empty($sOrdem)) {
                  $sSQL .= ' ORDER BY '.$sOrdem;
                }

                if ($bPaginar) {
                  require_once('../inc/class.paginador.php');

                  $sSQLCount = 'SELECT COUNT(1) AS total ' . $sWhere;
                  $oRes = $this->oDBAP->Execute($sSQLCount);

                  if ($oRes === false) {
                    $this->sErro = $this->oDBAP->ErrorMsg() . ' - ' . $sSQLCount;
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }

                  $iTotalResultados = $oRes->fields['total'];

                  $iOffset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;

                  $this->oPaginador = new paginador($iTotalResultados, $GLOBALS['CFGnrlinhas'], $iOffset);
                }

                if ($bPaginar && $iTotalResultados >= $GLOBALS['CFGnrlinhas'] && $iOffset != -1) { // fazer Paginacao
                  $oRes = $this->oDBAP->SelectLimit($sSQL, $GLOBALS['CFGnrlinhas'], $iOffset);
                } else {
                  $oRes = $this->oDBAP->Execute($sSQL);
                }

                if ($oRes === false) {
                  $this->sErro = $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                $this->iLinhas = $oRes->RecordCount();

                while (!$oRes->EOF) {

                  $this->ID_MATERIAL[]  = $oRes->fields['id_material'];
                  $this->NM_MATERIAL[]  = $oRes->fields['nm_material'];
                  $this->DE_MATERIAL[]  = $oRes->fields['de_material'];

                  $oRes->MoveNext();
                }
                $oRes->Close();

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::critica() - Realiza a cr�tica para inser��o de dados na tabela
               *
               * Realiza a cr�tica para inser��o de dados na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @param   string  $sFuncao
               * @return  void
               **/
              private function critica($sFuncao) {

                if ($sFuncao == 'incluir') {

                  $sSQL = 'SELECT id_material
                             FROM apcmateriais
                            WHERE id_material = ' . $this->oUtil->parseValue($this->ID_MATERIAL[0], '');

                  $oRes = $this->oDBAP->Execute($sSQL);
                  if ($oRes === false) {
                    $this->sErro = __('Erro na cr�tica.') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }
                  if ($oRes->RecordCount() > 0) {
                    $this->sErro = __('J� existe um registro com este c�digo.');
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }
                  $oRes->Close();
                }

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::insere() - Insere os dados da classe na tabela
               *
               * Insere os dados da classe na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @return  boolean
               **/
              private function insere() {

                $this->ID_MATERIAL[0] = $this->oUtil->getNextSeq($this->oDBAP, 'ap', 'apcmateriais', 'id_material', $iNumSeqs = 1);

                //$this->oDBAP->BeginTrans();

                $sSQL = 'INSERT INTO apcmateriais
                         (id_material,
                          nm_material,
                          de_material
                         )
                         VALUES
                         (' . $this->oUtil->parseValue($this->ID_MATERIAL[0], 'nu') . ',
                          ' . $this->oUtil->parseValue($this->NM_MATERIAL[0], 'va') . ',
                          ' . $this->oUtil->parseValue($this->DE_MATERIAL[0], 'va') . '
                         )';

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro incluindo registro') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                //$this->oDBAP->CommitTrans();

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::atualiza() - Atualiza os dados da na tabela
               *
               * Atualiza os dados na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @param   $sFuncao Se a a��o ser� 'atualizar' ou 'incluir'
               * @return  boolean
               **/
              public function atualiza($sFuncao = 'atualizar') {

                if (!$this->critica($sFuncao)) {
                  return false;
                }

                if ($sFuncao == 'incluir') {
                  return $this->insere();
                }

                //$this->oDBAP->BeginTrans();

                $sSQL = 'UPDATE apcmateriais
                            SET nm_material  = ' . $this->oUtil->parseValue($this->NM_MATERIAL[0], 'va') . ',
                                de_material  = ' . $this->oUtil->parseValue($this->DE_MATERIAL[0], 'va') . '
                          WHERE id_material = ' . $this->oUtil->parseValue($this->ID_MATERIAL[0], 'nu');

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro na atualiza��o do registro.') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                //$this->oDBAP->CommitTrans();
                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::remove() - Deleta os dados da tabela
               *
               * Deleta os dados da tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @return  boolean Se deletou com sucesso ou n�o
               **/
              public function remove($sId) {

                if (empty($sId)) {
                  $this->sErro = __('Identificador inv�lido para exclus�o') . '!';
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                $sFiltro = $this->oUtil->makeWhereFromKeyStr($sId,"");

                // Verifica se o material tem alguma condi��o vinculada, caso positivo retorna erro
                include('../GC/class.gcrcondmat.php');
                $oCondMat = new gcrcondmat($sFiltro);
                if( $oCondMat->iLinhas == 1 ) {
                  $this->sErro = __('J� existe uma condi��o vinculada ao material') . '!';
                  return false;
                } elseif ( $oCondMat->iLinhas > 1 ) {
                  $this->sErro = __('J� existem mais de uma condi��o vinculada aos materiais') . '!';
                  return false;
                }

                //$this->oDBAP->BeginTrans();

                $sSQL = 'DELETE FROM apcmateriais
                         WHERE ' . $sFiltro;

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro na exclus�o do registro nas ocorr�ncias') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }
                echo '<pre>';
                var_dump($sSQL);
                echo '</pre>';

                //$this->oDBAP->CommitTrans();
                unset($this->sErro);
                return true;
              }
            }
          ?>
          <?php
            /**
             * Descricao
             *
             * @package    
             * @author     Alex <alex@lunacom.com.br>
             * @copyright  
             * @date
             **/

            /*

            INCLUIR OS ARQUIVOS NECESS�RIOS PARA CONEXAO COM BANCO DE DADOS

            if (basename($_SERVER['PHP_SELF']) == 'class.apcmateriais.php' || $_SERVER['PHP_SELF'] == '') {
              require_once('../inc/class.redirecionaParaURL.php');
              redirecionaParaURL('../index.php');
              exit;
            }

            require_once($helpSkin.$sPrefInc.'../inc/LangMan/lmutil.php');
            bindLanguage('Ditech', $idiomaSessao);
            */


            /**
             * Descricao
             *
             * @package    
             * @copyright  
             **/
            class teste {

              /**
               * Conex�o com o banco onde est� a tabela
               * @var object $oDB
               */
              private $oDB;

              /**
               * Fun��es da Util
               * @var object $oUtil
               */
              private $oUtil;

              /**
               * Linhas encontradas pelo filtro do construtor
               * @var intenger $iLinhas
               */
              public $iLinhas = 0;

              /**
               * Armazena o �ltimo erro ocorrido na classe
               * @var string $sErro
               */
              public $sErro = '';

              /**
               * Paginador dos resultados
               * @var object $oPaginador
               */
              public $oPaginador;

              /**
               * Campos da tabela
               */      

              /**
               * teste::teste() - Construtor da classe
               *
               * @author  Alex <alex@lunacom.com.br>
               * @param   string  $sFiltro Filtro para popular os dados da classe
               * @return  void
               **/
              public function __construct($sFiltro = '', $bPaginar = false, $sOrdem = '') {

                if (isset($GLOBALS['util'])) {
                  $this->oUtil =& $GLOBALS['util'];
                } else {
                  $this->oUtil =& $GLOBALS['oUtil'];
                }
                $this->oDBAP =& $GLOBALS['DBAP'];
                // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex

                if (!empty($sFiltro)) {
                  return $this->consultar($sFiltro, $bPaginar, $sOrdem);
                }
              }

              /**
               * teste::consultar() - Armazena os dados da tabela na classe
               *
               * Armazena os dados da tabela na classe
               *
               * @author  Alex <alex@lunacom.com.br>
               * @param   string  $sFiltro Filtro ao trazer os dados da tabela
               * @param   boolean $bPaginar Booleano que indica se deve paginar os resultados
               * @param   string  $sOrdem Campo que ser� ordenado na cl�usula SQL
               * @return  boolean Se a consulta retornou com ou sem erro
               **/
              public function consultar($sFiltro, $bPaginar = false, $sOrdem = '') {

                $sSQL = 'SELECT apcmateriais.id_material,
                                apcmateriais.nm_material,
                                apcmateriais.de_material
                           ';

                $sWhere = 'FROM apcmateriais
                          WHERE (' . $sFiltro . ')';

                $sSQL .= $sWhere;

                if(!empty($sOrdem)) {
                  $sSQL .= ' ORDER BY '.$sOrdem;
                }

                if ($bPaginar) {
                  require_once('../inc/class.paginador.php');

                  $sSQLCount = 'SELECT COUNT(1) AS total ' . $sWhere;
                  $oRes = $this->oDBAP->Execute($sSQLCount);

                  if ($oRes === false) {
                    $this->sErro = $this->oDBAP->ErrorMsg() . ' - ' . $sSQLCount;
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }

                  $iTotalResultados = $oRes->fields['total'];

                  $iOffset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;

                  $this->oPaginador = new paginador($iTotalResultados, $GLOBALS['CFGnrlinhas'], $iOffset);
                }

                if ($bPaginar && $iTotalResultados >= $GLOBALS['CFGnrlinhas'] && $iOffset != -1) { // fazer Paginacao
                  $oRes = $this->oDBAP->SelectLimit($sSQL, $GLOBALS['CFGnrlinhas'], $iOffset);
                } else {
                  $oRes = $this->oDBAP->Execute($sSQL);
                }

                if ($oRes === false) {
                  $this->sErro = $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                $this->iLinhas = $oRes->RecordCount();

                while (!$oRes->EOF) {

                  $this->ID_MATERIAL[]  = $oRes->fields['id_material'];
                  $this->NM_MATERIAL[]  = $oRes->fields['nm_material'];
                  $this->DE_MATERIAL[]  = $oRes->fields['de_material'];

                  $oRes->MoveNext();
                }
                $oRes->Close();

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::critica() - Realiza a cr�tica para inser��o de dados na tabela
               *
               * Realiza a cr�tica para inser��o de dados na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @param   string  $sFuncao
               * @return  void
               **/
              private function critica($sFuncao) {

                if ($sFuncao == 'incluir') {

                  $sSQL = 'SELECT id_material
                             FROM apcmateriais
                            WHERE id_material = ' . $this->oUtil->parseValue($this->ID_MATERIAL[0], '');

                  $oRes = $this->oDBAP->Execute($sSQL);
                  if ($oRes === false) {
                    $this->sErro = __('Erro na cr�tica.') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }
                  if ($oRes->RecordCount() > 0) {
                    $this->sErro = __('J� existe um registro com este c�digo.');
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }
                  $oRes->Close();
                }

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::insere() - Insere os dados da classe na tabela
               *
               * Insere os dados da classe na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @return  boolean
               **/
              private function insere() {

                $this->ID_MATERIAL[0] = $this->oUtil->getNextSeq($this->oDBAP, 'ap', 'apcmateriais', 'id_material', $iNumSeqs = 1);

                //$this->oDBAP->BeginTrans();

                $sSQL = 'INSERT INTO apcmateriais
                         (id_material,
                          nm_material,
                          de_material
                         )
                         VALUES
                         (' . $this->oUtil->parseValue($this->ID_MATERIAL[0], 'nu') . ',
                          ' . $this->oUtil->parseValue($this->NM_MATERIAL[0], 'va') . ',
                          ' . $this->oUtil->parseValue($this->DE_MATERIAL[0], 'va') . '
                         )';

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro incluindo registro') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                //$this->oDBAP->CommitTrans();

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::atualiza() - Atualiza os dados da na tabela
               *
               * Atualiza os dados na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @param   $sFuncao Se a a��o ser� 'atualizar' ou 'incluir'
               * @return  boolean
               **/
              public function atualiza($sFuncao = 'atualizar') {

                if (!$this->critica($sFuncao)) {
                  return false;
                }

                if ($sFuncao == 'incluir') {
                  return $this->insere();
                }

                //$this->oDBAP->BeginTrans();

                $sSQL = 'UPDATE apcmateriais
                            SET nm_material  = ' . $this->oUtil->parseValue($this->NM_MATERIAL[0], 'va') . ',
                                de_material  = ' . $this->oUtil->parseValue($this->DE_MATERIAL[0], 'va') . '
                          WHERE id_material = ' . $this->oUtil->parseValue($this->ID_MATERIAL[0], 'nu');

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro na atualiza��o do registro.') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                //$this->oDBAP->CommitTrans();
                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::remove() - Deleta os dados da tabela
               *
               * Deleta os dados da tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @return  boolean Se deletou com sucesso ou n�o
               **/
              public function remove($sId) {

                if (empty($sId)) {
                  $this->sErro = __('Identificador inv�lido para exclus�o') . '!';
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                $sFiltro = $this->oUtil->makeWhereFromKeyStr($sId,"");

                // Verifica se o material tem alguma condi��o vinculada, caso positivo retorna erro
                include('../GC/class.gcrcondmat.php');
                $oCondMat = new gcrcondmat($sFiltro);
                if( $oCondMat->iLinhas == 1 ) {
                  $this->sErro = __('J� existe uma condi��o vinculada ao material') . '!';
                  return false;
                } elseif ( $oCondMat->iLinhas > 1 ) {
                  $this->sErro = __('J� existem mais de uma condi��o vinculada aos materiais') . '!';
                  return false;
                }

                //$this->oDBAP->BeginTrans();

                $sSQL = 'DELETE FROM apcmateriais
                         WHERE ' . $sFiltro;

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro na exclus�o do registro nas ocorr�ncias') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }
                echo '<pre>';
                var_dump($sSQL);
                echo '</pre>';

                //$this->oDBAP->CommitTrans();
                unset($this->sErro);
                return true;
              }
            }
          ?>
          <?php
            /**
             * Descricao
             *
             * @package    
             * @author     Alex <alex@lunacom.com.br>
             * @copyright  
             * @date
             **/

            /*

            INCLUIR OS ARQUIVOS NECESS�RIOS PARA CONEXAO COM BANCO DE DADOS

            if (basename($_SERVER['PHP_SELF']) == 'class.apcmateriais.php' || $_SERVER['PHP_SELF'] == '') {
              require_once('../inc/class.redirecionaParaURL.php');
              redirecionaParaURL('../index.php');
              exit;
            }

            require_once($helpSkin.$sPrefInc.'../inc/LangMan/lmutil.php');
            bindLanguage('Ditech', $idiomaSessao);
            */


            /**
             * Descricao
             *
             * @package    
             * @copyright  
             **/
            class teste {

              /**
               * Conex�o com o banco onde est� a tabela
               * @var object $oDB
               */
              private $oDB;

              /**
               * Fun��es da Util
               * @var object $oUtil
               */
              private $oUtil;

              /**
               * Linhas encontradas pelo filtro do construtor
               * @var intenger $iLinhas
               */
              public $iLinhas = 0;

              /**
               * Armazena o �ltimo erro ocorrido na classe
               * @var string $sErro
               */
              public $sErro = '';

              /**
               * Paginador dos resultados
               * @var object $oPaginador
               */
              public $oPaginador;

              /**
               * Campos da tabela
               */      

              /**
               * teste::teste() - Construtor da classe
               *
               * @author  Alex <alex@lunacom.com.br>
               * @param   string  $sFiltro Filtro para popular os dados da classe
               * @return  void
               **/
              public function __construct($sFiltro = '', $bPaginar = false, $sOrdem = '') {
                // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex

                if (isset($GLOBALS['util'])) {
                  $this->oUtil =& $GLOBALS['util'];
                } else {
                  $this->oUtil =& $GLOBALS['oUtil'];
                }
                $this->oDBAP =& $GLOBALS['DBAP'];
                // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex

                if (!empty($sFiltro)) {
                  return $this->consultar($sFiltro, $bPaginar, $sOrdem);
                }
              }

              /**
               * teste::consultar() - Armazena os dados da tabela na classe
               *
               * Armazena os dados da tabela na classe
               *
               * @author  Alex <alex@lunacom.com.br>
               * @param   string  $sFiltro Filtro ao trazer os dados da tabela
               * @param   boolean $bPaginar Booleano que indica se deve paginar os resultados
               * @param   string  $sOrdem Campo que ser� ordenado na cl�usula SQL
               * @return  boolean Se a consulta retornou com ou sem erro
               **/
              public function consultar($sFiltro, $bPaginar = false, $sOrdem = '') {

                $sSQL = 'SELECT apcmateriais.id_material,
                                apcmateriais.nm_material,
                                apcmateriais.de_material
                           ';

                $sWhere = 'FROM apcmateriais
                          WHERE (' . $sFiltro . ')';

                $sSQL .= $sWhere;

                if(!empty($sOrdem)) {
                  $sSQL .= ' ORDER BY '.$sOrdem;
                }

                if ($bPaginar) {
                  require_once('../inc/class.paginador.php');

                  $sSQLCount = 'SELECT COUNT(1) AS total ' . $sWhere;
                  $oRes = $this->oDBAP->Execute($sSQLCount);

                  if ($oRes === false) {
                    $this->sErro = $this->oDBAP->ErrorMsg() . ' - ' . $sSQLCount;
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }

                  $iTotalResultados = $oRes->fields['total'];

                  $iOffset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;

                  $this->oPaginador = new paginador($iTotalResultados, $GLOBALS['CFGnrlinhas'], $iOffset);
                }

                if ($bPaginar && $iTotalResultados >= $GLOBALS['CFGnrlinhas'] && $iOffset != -1) { // fazer Paginacao
                  $oRes = $this->oDBAP->SelectLimit($sSQL, $GLOBALS['CFGnrlinhas'], $iOffset);
                } else {
                  $oRes = $this->oDBAP->Execute($sSQL);
                }

                if ($oRes === false) {
                  $this->sErro = $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                $this->iLinhas = $oRes->RecordCount();

                while (!$oRes->EOF) {

                  $this->ID_MATERIAL[]  = $oRes->fields['id_material'];
                  $this->NM_MATERIAL[]  = $oRes->fields['nm_material'];
                  $this->DE_MATERIAL[]  = $oRes->fields['de_material'];

                  $oRes->MoveNext();
                }
                $oRes->Close();

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::critica() - Realiza a cr�tica para inser��o de dados na tabela
               *
               * Realiza a cr�tica para inser��o de dados na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @param   string  $sFuncao
               * @return  void
               **/
              private function critica($sFuncao) {

                if ($sFuncao == 'incluir') {

                  $sSQL = 'SELECT id_material
                             FROM apcmateriais
                            WHERE id_material = ' . $this->oUtil->parseValue($this->ID_MATERIAL[0], '');

                  $oRes = $this->oDBAP->Execute($sSQL);
                  if ($oRes === false) {
                    $this->sErro = __('Erro na cr�tica.') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }
                  if ($oRes->RecordCount() > 0) {
                    $this->sErro = __('J� existe um registro com este c�digo.');
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }
                  $oRes->Close();
                }

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::insere() - Insere os dados da classe na tabela
               *
               * Insere os dados da classe na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @return  boolean
               **/
              private function insere() {

                $this->ID_MATERIAL[0] = $this->oUtil->getNextSeq($this->oDBAP, 'ap', 'apcmateriais', 'id_material', $iNumSeqs = 1);

                //$this->oDBAP->BeginTrans();

                $sSQL = 'INSERT INTO apcmateriais
                         (id_material,
                          nm_material,
                          de_material
                         )
                         VALUES
                         (' . $this->oUtil->parseValue($this->ID_MATERIAL[0], 'nu') . ',
                          ' . $this->oUtil->parseValue($this->NM_MATERIAL[0], 'va') . ',
                          ' . $this->oUtil->parseValue($this->DE_MATERIAL[0], 'va') . '
                         )';

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro incluindo registro') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                //$this->oDBAP->CommitTrans();

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::atualiza() - Atualiza os dados da na tabela
               *
               * Atualiza os dados na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @param   $sFuncao Se a a��o ser� 'atualizar' ou 'incluir'
               * @return  boolean
               **/
              public function atualiza($sFuncao = 'atualizar') {

                if (!$this->critica($sFuncao)) {
                  return false;
                }

                if ($sFuncao == 'incluir') {
                  return $this->insere();
                }

                //$this->oDBAP->BeginTrans();

                $sSQL = 'UPDATE apcmateriais
                            SET nm_material  = ' . $this->oUtil->parseValue($this->NM_MATERIAL[0], 'va') . ',
                                de_material  = ' . $this->oUtil->parseValue($this->DE_MATERIAL[0], 'va') . '
                          WHERE id_material = ' . $this->oUtil->parseValue($this->ID_MATERIAL[0], 'nu');

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro na atualiza��o do registro.') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                //$this->oDBAP->CommitTrans();
                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::remove() - Deleta os dados da tabela
               *
               * Deleta os dados da tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @return  boolean Se deletou com sucesso ou n�o
               **/
              public function remove($sId) {

                if (empty($sId)) {
                  $this->sErro = __('Identificador inv�lido para exclus�o') . '!';
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                $sFiltro = $this->oUtil->makeWhereFromKeyStr($sId,"");

                // Verifica se o material tem alguma condi��o vinculada, caso positivo retorna erro
                include('../GC/class.gcrcondmat.php');
                $oCondMat = new gcrcondmat($sFiltro);
                if( $oCondMat->iLinhas == 1 ) {
                  $this->sErro = __('J� existe uma condi��o vinculada ao material') . '!';
                  return false;
                } elseif ( $oCondMat->iLinhas > 1 ) {
                  $this->sErro = __('J� existem mais de uma condi��o vinculada aos materiais') . '!';
                  return false;
                }

                //$this->oDBAP->BeginTrans();

                $sSQL = 'DELETE FROM apcmateriais
                         WHERE ' . $sFiltro;

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro na exclus�o do registro nas ocorr�ncias') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }
                echo '<pre>';
                var_dump($sSQL);
                echo '</pre>';

                //$this->oDBAP->CommitTrans();
                unset($this->sErro);
                return true;
              }
            }
          ?>
          <?php
            /**
             * Descricao
             *
             * @package    
             * @author     Alex <alex@lunacom.com.br>
             * @copyright  
             * @date
             **/

            /*

            INCLUIR OS ARQUIVOS NECESS�RIOS PARA CONEXAO COM BANCO DE DADOS

            if (basename($_SERVER['PHP_SELF']) == 'class.apcmateriais.php' || $_SERVER['PHP_SELF'] == '') {
              require_once('../inc/class.redirecionaParaURL.php');
              redirecionaParaURL('../index.php');
              exit;
            }

            require_once($helpSkin.$sPrefInc.'../inc/LangMan/lmutil.php');
            bindLanguage('Ditech', $idiomaSessao);
            */


            /**
             * Descricao
             *
             * @package    
             * @copyright  
             **/
            class teste {

              /**
               * Conex�o com o banco onde est� a tabela
               * @var object $oDB
               */
              private $oDB;

              /**
               * Fun��es da Util
               * @var object $oUtil
               */
              private $oUtil;

              /**
               * Linhas encontradas pelo filtro do construtor
               * @var intenger $iLinhas
               */
              public $iLinhas = 0;

              /**
               * Armazena o �ltimo erro ocorrido na classe
               * @var string $sErro
               */
              public $sErro = '';

              /**
               * Paginador dos resultados
               * @var object $oPaginador
               */
              public $oPaginador;

              /**
               * Campos da tabela
               */      

              /**
               * teste::teste() - Construtor da classe
               *
               * @author  Alex <alex@lunacom.com.br>
               * @param   string  $sFiltro Filtro para popular os dados da classe
               * @return  void
               **/
              public function __construct($sFiltro = '', $bPaginar = false, $sOrdem = '') {
                // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex

                if (isset($GLOBALS['util'])) {
                  $this->oUtil =& $GLOBALS['util'];
                } else {
                  $this->oUtil =& $GLOBALS['oUtil'];
                }
                $this->oDBAP =& $GLOBALS['DBAP'];
                // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex

                if (!empty($sFiltro)) {
                  return $this->consultar($sFiltro, $bPaginar, $sOrdem);
                }
              }

              /**
               * teste::consultar() - Armazena os dados da tabela na classe
               *
               * Armazena os dados da tabela na classe
               *
               * @author  Alex <alex@lunacom.com.br>
               * @param   string  $sFiltro Filtro ao trazer os dados da tabela
               * @param   boolean $bPaginar Booleano que indica se deve paginar os resultados
               * @param   string  $sOrdem Campo que ser� ordenado na cl�usula SQL
               * @return  boolean Se a consulta retornou com ou sem erro
               **/
              public function consultar($sFiltro, $bPaginar = false, $sOrdem = '') {

                $sSQL = 'SELECT apcmateriais.id_material,
                                apcmateriais.nm_material,
                                apcmateriais.de_material
                           ';

                $sWhere = 'FROM apcmateriais
                          WHERE (' . $sFiltro . ')';

                $sSQL .= $sWhere;

                if(!empty($sOrdem)) {
                  $sSQL .= ' ORDER BY '.$sOrdem;
                }

                if ($bPaginar) {
                  require_once('../inc/class.paginador.php');

                  $sSQLCount = 'SELECT COUNT(1) AS total ' . $sWhere;
                  $oRes = $this->oDBAP->Execute($sSQLCount);

                  if ($oRes === false) {
                    $this->sErro = $this->oDBAP->ErrorMsg() . ' - ' . $sSQLCount;
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }

                  $iTotalResultados = $oRes->fields['total'];

                  $iOffset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;

                  $this->oPaginador = new paginador($iTotalResultados, $GLOBALS['CFGnrlinhas'], $iOffset);
                }

                if ($bPaginar && $iTotalResultados >= $GLOBALS['CFGnrlinhas'] && $iOffset != -1) { // fazer Paginacao
                  $oRes = $this->oDBAP->SelectLimit($sSQL, $GLOBALS['CFGnrlinhas'], $iOffset);
                } else {
                  $oRes = $this->oDBAP->Execute($sSQL);
                }

                if ($oRes === false) {
                  $this->sErro = $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                $this->iLinhas = $oRes->RecordCount();

                while (!$oRes->EOF) {

                  $this->ID_MATERIAL[]  = $oRes->fields['id_material'];
                  $this->NM_MATERIAL[]  = $oRes->fields['nm_material'];
                  $this->DE_MATERIAL[]  = $oRes->fields['de_material'];

                  $oRes->MoveNext();
                }
                $oRes->Close();

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::critica() - Realiza a cr�tica para inser��o de dados na tabela
               *
               * Realiza a cr�tica para inser��o de dados na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @param   string  $sFuncao
               * @return  void
               **/
              private function critica($sFuncao) {

                if ($sFuncao == 'incluir') {

                  $sSQL = 'SELECT id_material
                             FROM apcmateriais
                            WHERE id_material = ' . $this->oUtil->parseValue($this->ID_MATERIAL[0], '');

                  $oRes = $this->oDBAP->Execute($sSQL);
                  if ($oRes === false) {
                    $this->sErro = __('Erro na cr�tica.') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }
                  if ($oRes->RecordCount() > 0) {
                    $this->sErro = __('J� existe um registro com este c�digo.');
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }
                  $oRes->Close();
                }

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::insere() - Insere os dados da classe na tabela
               *
               * Insere os dados da classe na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @return  boolean
               **/
              private function insere() {

                $this->ID_MATERIAL[0] = $this->oUtil->getNextSeq($this->oDBAP, 'ap', 'apcmateriais', 'id_material', $iNumSeqs = 1);

                //$this->oDBAP->BeginTrans();

                $sSQL = 'INSERT INTO apcmateriais
                         (id_material,
                          nm_material,
                          de_material
                         )
                         VALUES
                         (' . $this->oUtil->parseValue($this->ID_MATERIAL[0], 'nu') . ',
                          ' . $this->oUtil->parseValue($this->NM_MATERIAL[0], 'va') . ',
                          ' . $this->oUtil->parseValue($this->DE_MATERIAL[0], 'va') . '
                         )';

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro incluindo registro') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                //$this->oDBAP->CommitTrans();

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::atualiza() - Atualiza os dados da na tabela
               *
               * Atualiza os dados na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @param   $sFuncao Se a a��o ser� 'atualizar' ou 'incluir'
               * @return  boolean
               **/
              public function atualiza($sFuncao = 'atualizar') {

                if (!$this->critica($sFuncao)) {
                  return false;
                }

                if ($sFuncao == 'incluir') {
                  return $this->insere();
                }

                //$this->oDBAP->BeginTrans();

                $sSQL = 'UPDATE apcmateriais
                            SET nm_material  = ' . $this->oUtil->parseValue($this->NM_MATERIAL[0], 'va') . ',
                                de_material  = ' . $this->oUtil->parseValue($this->DE_MATERIAL[0], 'va') . '
                          WHERE id_material = ' . $this->oUtil->parseValue($this->ID_MATERIAL[0], 'nu');

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro na atualiza��o do registro.') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                //$this->oDBAP->CommitTrans();
                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::remove() - Deleta os dados da tabela
               *
               * Deleta os dados da tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @return  boolean Se deletou com sucesso ou n�o
               **/
              public function remove($sId) {

                if (empty($sId)) {
                  $this->sErro = __('Identificador inv�lido para exclus�o') . '!';
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                $sFiltro = $this->oUtil->makeWhereFromKeyStr($sId,"");

                // Verifica se o material tem alguma condi��o vinculada, caso positivo retorna erro
                include('../GC/class.gcrcondmat.php');
                $oCondMat = new gcrcondmat($sFiltro);
                if( $oCondMat->iLinhas == 1 ) {
                  $this->sErro = __('J� existe uma condi��o vinculada ao material') . '!';
                  return false;
                } elseif ( $oCondMat->iLinhas > 1 ) {
                  $this->sErro = __('J� existem mais de uma condi��o vinculada aos materiais') . '!';
                  return false;
                }

                //$this->oDBAP->BeginTrans();

                $sSQL = 'DELETE FROM apcmateriais
                         WHERE ' . $sFiltro;

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro na exclus�o do registro nas ocorr�ncias') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }
                echo '<pre>';
                var_dump($sSQL);
                echo '</pre>';

                //$this->oDBAP->CommitTrans();
                unset($this->sErro);
                return true;
              }
            }
          ?>
          <?php
            /**
             * Descricao
             *
             * @package    
             * @author     Alex <alex@lunacom.com.br>
             * @copyright  
             * @date
             **/

            /*

            INCLUIR OS ARQUIVOS NECESS�RIOS PARA CONEXAO COM BANCO DE DADOS

            if (basename($_SERVER['PHP_SELF']) == 'class.apcmateriais.php' || $_SERVER['PHP_SELF'] == '') {
              require_once('../inc/class.redirecionaParaURL.php');
              redirecionaParaURL('../index.php');
              exit;
            }

            require_once($helpSkin.$sPrefInc.'../inc/LangMan/lmutil.php');
            bindLanguage('Ditech', $idiomaSessao);
            */


            /**
             * Descricao
             *
             * @package    
             * @copyright  
             **/
            class teste {

              /**
               * Conex�o com o banco onde est� a tabela
               * @var object $oDB
               */
              private $oDB;

              /**
               * Fun��es da Util
               * @var object $oUtil
               */
              private $oUtil;

              /**
               * Linhas encontradas pelo filtro do construtor
               * @var intenger $iLinhas
               */
              public $iLinhas = 0;

              /**
               * Armazena o �ltimo erro ocorrido na classe
               * @var string $sErro
               */
              public $sErro = '';

              /**
               * Paginador dos resultados
               * @var object $oPaginador
               */
              public $oPaginador;

              /**
               * Campos da tabela
               */      

              /**
               * teste::teste() - Construtor da classe
               *
               * @author  Alex <alex@lunacom.com.br>
               * @param   string  $sFiltro Filtro para popular os dados da classe
               * @return  void
               **/
              public function __construct($sFiltro = '', $bPaginar = false, $sOrdem = '') {
                // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex
                  INSTANCIAR CLASSES NECESSARIAS PARA O OBJETO
                if (isset($GLOBALS['util'])) {
                  $this->oUtil =& $GLOBALS['util'];
                } else {
                  $this->oUtil =& $GLOBALS['oUtil'];
                }
                $this->oDBAP =& $GLOBALS['DBAP'];
                // @TODO - Este trecho foi adicionado com prop�sito de testes - tstAlex

                if (!empty($sFiltro)) {
                  return $this->consultar($sFiltro, $bPaginar, $sOrdem);
                }
              }

              /**
               * teste::consultar() - Armazena os dados da tabela na classe
               *
               * Armazena os dados da tabela na classe
               *
               * @author  Alex <alex@lunacom.com.br>
               * @param   string  $sFiltro Filtro ao trazer os dados da tabela
               * @param   boolean $bPaginar Booleano que indica se deve paginar os resultados
               * @param   string  $sOrdem Campo que ser� ordenado na cl�usula SQL
               * @return  boolean Se a consulta retornou com ou sem erro
               **/
              public function consultar($sFiltro, $bPaginar = false, $sOrdem = '') {

                $sSQL = 'SELECT apcmateriais.id_material,
                                apcmateriais.nm_material,
                                apcmateriais.de_material
                           ';

                $sWhere = 'FROM apcmateriais
                          WHERE (' . $sFiltro . ')';

                $sSQL .= $sWhere;

                if(!empty($sOrdem)) {
                  $sSQL .= ' ORDER BY '.$sOrdem;
                }

                if ($bPaginar) {
                  require_once('../inc/class.paginador.php');

                  $sSQLCount = 'SELECT COUNT(1) AS total ' . $sWhere;
                  $oRes = $this->oDBAP->Execute($sSQLCount);

                  if ($oRes === false) {
                    $this->sErro = $this->oDBAP->ErrorMsg() . ' - ' . $sSQLCount;
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }

                  $iTotalResultados = $oRes->fields['total'];

                  $iOffset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;

                  $this->oPaginador = new paginador($iTotalResultados, $GLOBALS['CFGnrlinhas'], $iOffset);
                }

                if ($bPaginar && $iTotalResultados >= $GLOBALS['CFGnrlinhas'] && $iOffset != -1) { // fazer Paginacao
                  $oRes = $this->oDBAP->SelectLimit($sSQL, $GLOBALS['CFGnrlinhas'], $iOffset);
                } else {
                  $oRes = $this->oDBAP->Execute($sSQL);
                }

                if ($oRes === false) {
                  $this->sErro = $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                $this->iLinhas = $oRes->RecordCount();

                while (!$oRes->EOF) {

                  $this->ID_MATERIAL[]  = $oRes->fields['id_material'];
                  $this->NM_MATERIAL[]  = $oRes->fields['nm_material'];
                  $this->DE_MATERIAL[]  = $oRes->fields['de_material'];

                  $oRes->MoveNext();
                }
                $oRes->Close();

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::critica() - Realiza a cr�tica para inser��o de dados na tabela
               *
               * Realiza a cr�tica para inser��o de dados na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @param   string  $sFuncao
               * @return  void
               **/
              private function critica($sFuncao) {

                if ($sFuncao == 'incluir') {

                  $sSQL = 'SELECT id_material
                             FROM apcmateriais
                            WHERE id_material = ' . $this->oUtil->parseValue($this->ID_MATERIAL[0], '');

                  $oRes = $this->oDBAP->Execute($sSQL);
                  if ($oRes === false) {
                    $this->sErro = __('Erro na cr�tica.') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }
                  if ($oRes->RecordCount() > 0) {
                    $this->sErro = __('J� existe um registro com este c�digo.');
                    trigger_error($this->sErro, E_USER_ERROR);
                    return false;
                  }
                  $oRes->Close();
                }

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::insere() - Insere os dados da classe na tabela
               *
               * Insere os dados da classe na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @return  boolean
               **/
              private function insere() {

                $this->ID_MATERIAL[0] = $this->oUtil->getNextSeq($this->oDBAP, 'ap', 'apcmateriais', 'id_material', $iNumSeqs = 1);

                //$this->oDBAP->BeginTrans();

                $sSQL = 'INSERT INTO apcmateriais
                         (id_material,
                          nm_material,
                          de_material
                         )
                         VALUES
                         (' . $this->oUtil->parseValue($this->ID_MATERIAL[0], 'nu') . ',
                          ' . $this->oUtil->parseValue($this->NM_MATERIAL[0], 'va') . ',
                          ' . $this->oUtil->parseValue($this->DE_MATERIAL[0], 'va') . '
                         )';

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro incluindo registro') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                //$this->oDBAP->CommitTrans();

                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::atualiza() - Atualiza os dados da na tabela
               *
               * Atualiza os dados na tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @param   $sFuncao Se a a��o ser� 'atualizar' ou 'incluir'
               * @return  boolean
               **/
              public function atualiza($sFuncao = 'atualizar') {

                if (!$this->critica($sFuncao)) {
                  return false;
                }

                if ($sFuncao == 'incluir') {
                  return $this->insere();
                }

                //$this->oDBAP->BeginTrans();

                $sSQL = 'UPDATE apcmateriais
                            SET nm_material  = ' . $this->oUtil->parseValue($this->NM_MATERIAL[0], 'va') . ',
                                de_material  = ' . $this->oUtil->parseValue($this->DE_MATERIAL[0], 'va') . '
                          WHERE id_material = ' . $this->oUtil->parseValue($this->ID_MATERIAL[0], 'nu');

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro na atualiza��o do registro.') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                //$this->oDBAP->CommitTrans();
                unset($this->sErro);
                return true;
              }

              /**
               * apcmateriais::remove() - Deleta os dados da tabela
               *
               * Deleta os dados da tabela
               *
               * @author  Alex Lunardelli <alex@ditech.com.br>
               * @return  boolean Se deletou com sucesso ou n�o
               **/
              public function remove($sId) {

                if (empty($sId)) {
                  $this->sErro = __('Identificador inv�lido para exclus�o') . '!';
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }

                $sFiltro = $this->oUtil->makeWhereFromKeyStr($sId,"");

                // Verifica se o material tem alguma condi��o vinculada, caso positivo retorna erro
                include('../GC/class.gcrcondmat.php');
                $oCondMat = new gcrcondmat($sFiltro);
                if( $oCondMat->iLinhas == 1 ) {
                  $this->sErro = __('J� existe uma condi��o vinculada ao material') . '!';
                  return false;
                } elseif ( $oCondMat->iLinhas > 1 ) {
                  $this->sErro = __('J� existem mais de uma condi��o vinculada aos materiais') . '!';
                  return false;
                }

                //$this->oDBAP->BeginTrans();

                $sSQL = 'DELETE FROM apcmateriais
                         WHERE ' . $sFiltro;

                if ($this->oDBAP->Execute($sSQL) === false) {
                  $this->sErro = __('Erro na exclus�o do registro nas ocorr�ncias') . ' - ' . $this->oDBAP->ErrorMsg() . ' - ' . $sSQL;
                  //$this->oDBAP->RollbackTrans();
                  trigger_error($this->sErro, E_USER_ERROR);
                  return false;
                }
                echo '<pre>';
                var_dump($sSQL);
                echo '</pre>';

                //$this->oDBAP->CommitTrans();
                unset($this->sErro);
                return true;
              }
            }
          ?>